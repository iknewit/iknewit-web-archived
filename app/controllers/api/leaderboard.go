// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

package controllers

import (
	"fmt"
	"github.com/revel/revel"
	cb "iknewit-web/app/controllers/api/base"
	"iknewit-web/app/services/leaderboard"
	"math"
)

//** TYPES

type (
	// League controller the League api
	Leaderboard struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Leaderboard).Before, revel.BEFORE)
	revel.InterceptMethod((*Leaderboard).After, revel.AFTER)
	revel.InterceptMethod((*Leaderboard).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

func (this *Leaderboard) List(size, page int) revel.Result {

	Leaderboard, err := leaderboard.ListLeaderboard(this.Services(), int(math.Min(float64(30), float64(size))), page)
	if err != nil {
		return this.RenderText(err.Error())
	}
	fmt.Printf("%s\n", "Leaderboard")

	return this.RenderJson(Leaderboard)
}
