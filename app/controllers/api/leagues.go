// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	LeagueController is implementing the League specific api

	http://localhost:9000/region/Gulf%20Of%20Mexico
	http://localhost:9000/station/42002
*/
package controllers

import (
	"github.com/revel/revel"
	cb "iknewit-web/app/controllers/api/base"
	"iknewit-web/app/services/league"
)

//** TYPES

type (
	// League controller the League api
	Leagues struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Leagues).Before, revel.BEFORE)
	revel.InterceptMethod((*Leagues).After, revel.AFTER)
	revel.InterceptMethod((*Leagues).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS
func (this *Leagues) List() revel.Result {
	leagues, err := league.FindAllLeagues(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(leagues)
}
