package controllers

import (
	"github.com/revel/revel"
	/* "gopkg.in/mgo.v2" */)

type Status struct {
	*revel.Controller
}

type StatusObj struct {
	Version                string
	AssetVersion           string
	AssetPath              string
	TermsPath              string
	PrivacyPolicyPath      string
	UseTeamIconPlaceHolder bool
}

func (c Status) Index() revel.Result {
	Status := &StatusObj{Version: "1.0", AssetVersion: "1.0",
		AssetPath: "TBC", TermsPath: "http://www.sayiknewit.com/terms",
		PrivacyPolicyPath: "http://www.sayiknewit.com/privacy", UseTeamIconPlaceHolder: false}
	return c.RenderJson(Status)
}
