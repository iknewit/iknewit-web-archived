package controllers

import (
	cb "iknewit-web/app/controllers/api/base"
	//"code.google.com/p/go.crypto/bcrypt"
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"iknewit-web/app/models/user"
	"iknewit-web/app/services/user"
	/*"image"
	  "bytes"
	  _ "image/jpeg"
	  _ "image/png"*/
	//"bytes"
	"io/ioutil"
	"path/filepath"
	//"net/http"
	"bytes"
	"github.com/kyokomi/cloudinary"
	"golang.org/x/net/context"
	"io"
	"os"
)

type (
	Users struct {
		cb.BaseController
	}
)

type FileInfo struct {
	ContentType string
	Filename    string
	RealFormat  string `json:",omitempty"`
	Resolution  string `json:",omitempty"`
	Size        int
	Status      string `json:",omitempty"`
}

const (
	_      = iota
	KB int = 1 << (10 * iota)
	MB
	GB
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Users).Before, revel.BEFORE)
	revel.InterceptMethod((*Users).After, revel.AFTER)
	revel.InterceptMethod((*Users).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

func (this *Users) ConfirmEmail(userId string) revel.Result {

	err := userService.ConfirmEmail(this.Services(), userId)

	if err != nil {
		return this.Forbidden(err.Error())
	}

	return this.Redirect("IKnewIt://")
}

func (this *Users) UpdateDisplayName() revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	fmt.Printf("%s\n", this.Request.ContentType)

	var userData userModel.User

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &userData)

	if err != nil {
		return this.RenderText("Unmarshal Error : " + err.Error())
	}

	userProfile, err2 := userService.UpdateDisplayName(this.Services(), userId, userData)
	_ = userProfile

	if err2 != nil {
		return this.Forbidden(err2.Error())
	}

	return this.RenderText("Success")
}

func (this *Users) GetUserProfile(targetuserid string) revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	//############## Validate targetuserid

	if targetuserid == "" {
		return this.Forbidden("Forbidden")
	}

	//##############

	fmt.Printf("%s\n", this.Request.ContentType)

	userProfile, err2 := userService.GetUserProfile(this.Services(), targetuserid)
	_ = userProfile

	if err2 != nil {
		return this.Forbidden(err2.Error())
	}

	return this.RenderJson(userProfile)
}

func (this *Users) UpdatePassword() revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	fmt.Printf("%s\n", this.Request.ContentType)

	var userPassword userModel.UserPassword

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &userPassword)

	if err != nil {
		return this.RenderText("Unmarshal Error : " + err.Error())
	}

	err2 := userService.UpdatePassword(this.Services(), userId, &userPassword)

	if err2 != nil {
		return this.Forbidden(err2.Error())
	}

	return this.RenderText("Success")
}

/*
func (this *Users) UpdateProfilePhoto(photo []byte) revel.Result {
    // Validation rules.
    this.Validation.Required(photo)
    this.Validation.MinSize(photo, 2*KB).
        Message("Minimum a file size of 2KB expected")
    this.Validation.MaxSize(photo, 2*MB).
        Message("File cannot be larger than 2MB")

    // Check format of the file.
    conf, format, err := image.DecodeConfig(bytes.NewReader(photo))
    this.Validation.Required(err == nil).Key("photo").
        Message("Incorrect file format")
    this.Validation.Required(format == "jpeg" || format == "png").Key("photo").
        Message("JPEG or PNG file format is expected")

    // Check resolution.
    this.Validation.Required(conf.Height >= 150 && conf.Width >= 150).Key("photo").
        Message("Minimum allowed resolution is 150x150px")
/*
    if this.Validation.HasErrors() {
        this.Validation.Keep()
        this.FlashParams()
        // ToDo return message
        return this.NotFound("")
    }
pathRoot  := os.Getenv("PWD")
path := http.Dir(filepath.Join(pathRoot, "static"))

http.Handle("/", http.FileServer(path))





    fmt.Printf("%s\n",revel.BasePath)
    return this.RenderJson(FileInfo{
        ContentType: this.Params.Files["photo"][0].Header.Get("Content-Type"),
        Filename:    this.Params.Files["photo"][0].Filename,
        RealFormat:  format,
        Resolution:  fmt.Sprintf("%dx%d", conf.Width, conf.Height),
        Size:        len(photo),
        Status:      "Successfully uploaded",
    })
}
*/

func (this *Users) UpdateProfilePhoto() revel.Result {

	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############
	fmt.Printf("%s\n", this.Request.ContentType)

	fmt.Println("method: ", this.Request.Method)

	this.Request.ParseMultipartForm(32 << 20)
	file, handler, err := this.Request.FormFile("Photo")

	// Assume no file upload, reset file path
	if err != nil {
		fmt.Println(err)
		userProfile, err2 := userService.UpdateProfilePhoto(this.Services(), userId, "")
		if err2 != nil {
			return this.Forbidden(err2.Error())
		}

		return this.RenderJson(userProfile)
	}
	defer file.Close()

	// Generate filename
	fileName := userId + filepath.Ext(handler.Filename)

	fmt.Println(revel.BasePath + "/public/profilephoto/" + fileName)

	f, err := os.OpenFile(revel.BasePath+"/public/profilephoto/"+fileName, os.O_WRONLY|os.O_CREATE, 0666)

	if err != nil {
		fmt.Println(err)
		return this.RenderText(err.Error())
	}
	defer f.Close()
	io.Copy(f, file)

	userProfile, err2 := userService.UpdateProfilePhoto(this.Services(), userId, fileName)

	if err2 != nil {
		return this.Forbidden(err2.Error())
	}

	ctx := context.Background()
	ctx = cloudinary.NewContext(ctx, revel.Config.StringDefault("cloudinary.uri", ""))

	data, _ := ioutil.ReadFile(revel.BasePath+"/public/profilephoto/"+fileName)

	cloudinary.UploadStaticImage(ctx, fileName, bytes.NewBuffer(data))

	fmt.Println(cloudinary.ResourceURL(ctx, fileName))

	return this.RenderJson(userProfile)
}

func (this *Users) GetUserAchievement(targetuserid string) revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()

	_ = userId
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	//############## Validate targetuserid

	if targetuserid == "" {
		return this.Forbidden("Forbidden")
	}

	//##############

	userAchievement, err := userService.GetUserAchievement(this.Services(), targetuserid)
	_ = userAchievement

	if err != nil {
		return this.Forbidden(err.Error())
	}

	return this.RenderJson(userAchievement)
}
