// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	CompetitionsController is implementing the Competitions specific api

*/
package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	cb "iknewit-web/app/controllers/api/base"
	"iknewit-web/app/models/competition"
	"iknewit-web/app/services/competition"
	"io/ioutil"
)

//** TYPES

type (
	// competitions controller the Competitions api
	Competitions struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Competitions).Before, revel.BEFORE)
	revel.InterceptMethod((*Competitions).After, revel.AFTER)
	revel.InterceptMethod((*Competitions).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS
func (this *Competitions) List() revel.Result {
	competitions, err := competition.FindAllCompetitions(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(competitions)
}

func (this *Competitions) Create() revel.Result {

	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	var competitionData competitionModel.Competition

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &competitionData)

	fmt.Printf("%s\n", err.Error())

	err = competition.CreateCompetition(this.Services(), userId, competitionData)

	fmt.Printf(" called \n")
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(competitionData)
}

func (this *Competitions) Join() revel.Result {

	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("user id %s\n", userId)

	fmt.Printf("%s\n", accessError)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	var competitionData competitionModel.CompetitionShareCode

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("Json Content : %s\n", content)

	err := json.Unmarshal(content, &competitionData)

	fmt.Printf("Sharecode : %s\n", competitionData.ShareCode)
	fmt.Printf("Join \n")

	err = competition.JoinCompetition(this.Services(), userId, competitionData)

	fmt.Printf("Finished \n")
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(competitionData)
}
