// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

package controllers

import (
	cb "iknewit-web/app/controllers/api/base"
	//"iknewit-web/app/models/prediction"
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"iknewit-web/app/models/match"
	"iknewit-web/app/services/match"
	"iknewit-web/app/services/competition"
	"iknewit-web/app/services/team"
	"io/ioutil"
	"math"
)

//** TYPES

type (
	// League controller the League api
	Matches struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Matches).Before, revel.BEFORE)
	revel.InterceptMethod((*Matches).After, revel.AFTER)
	revel.InterceptMethod((*Matches).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

func (c Matches) Index() revel.Result {
	return c.Render()
}

func (this *Matches) List() revel.Result {
	matches, err := match.FindAllMatches(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(matches)
}

func (this *Matches) ListUpcomingMatches(filterBy string, size, page int) revel.Result {

	//############## VerifyAccessToken
	userId, accessError := this.VerifyAccessToken()
	_ = accessError
	//##############


	matches, err := match.ListUpcomingMatches(this.Services(), userId, filterBy, int(math.Min(float64(30), float64(size))), page)

	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(matches)
}

func (this *Matches) ListUserPredictionUpcomingMatches(targetuserid string, filterBy string, size, page int) revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	fmt.Printf("%s\n", userId)
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	//############## Validate targetuserid

	if targetuserid == "" {
		return this.Forbidden("Forbidden")
	}

	//##############

	matches, err := match.ListUserPredictionUpcomingMatches(this.Services(), targetuserid, filterBy, int(math.Min(float64(30), float64(size))), page)
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(matches)
}

func (this *Matches) ListPastMatches(filterBy string, size, page int) revel.Result {

	userId, accessError := this.VerifyAccessToken()
	_ = accessError

	matches, err := match.ListPastMatches(this.Services(), userId, filterBy, int(math.Min(float64(30), float64(size))), page)

	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(matches)
}

func (this *Matches) ListUserPredictionPastMatches(targetuserid string, filterBy string, size, page int) revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()

	_ = userId
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	//############## Validate targetuserid

	if targetuserid == "" {
		return this.Forbidden("Forbidden")
	}

	//##############

	matches, err := match.ListUserPredictionPastMatches(this.Services(), targetuserid, filterBy, int(math.Min(float64(30), float64(size))), page)
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(matches)
}

func (this *Matches) Predict() revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()

	_ = userId
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	fmt.Printf("%s\n", this.Request.ContentType)

	var matchData matchModel.Match

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &matchData)

	fmt.Printf("matchData.Id  : %s\n", matchData.Id)

	if err != nil {
		return this.RenderText("Unmarshal Error : " + err.Error())
	}

	err2 := match.Predict(this.Services(), userId, matchData)

	if err2 != nil {
			return this.NotFound("Match started or not found")
	}

	fmt.Printf("Prediction Type : %s\n", matchData.Predictions[0].PredictionType)

    //##############


	return this.RenderJson(matchData)
}

/*
func (this *App) SaveUser(user userModel.User, verifyPassword string) revel.Result {
    this.Validation.Required(verifyPassword)
	this.Validation.Required(verifyPassword == user.Password).
		Message("Password does not match")


	err := userService.SignUp(&user, this.Services())
	if err != nil {
		panic(err)
		return this.SignUp()
	}

	this.Session["user"] = user.Email
	this.Flash.Success("Welcome, " + user.DisplayName)
	return this.Redirect(routes.Leagues.Index())
}
*/


//** CONTROLLER FUNCTIONS
func (this *Matches) AddMatchForm() revel.Result {



	competitions, err := competition.FindAllCompetitions(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}

	teams, err1 := team.FindAllTeams(this.Services())
	if err1 != nil {
		return this.RenderText(err.Error())
	}
	return this.Render(teams, competitions)
}

//** CONTROLLER FUNCTIONS
func (this *Matches) AddMatch() revel.Result {

	var matchData matchModel.Match

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &matchData)
	if err != nil {
		return this.RenderText(err.Error())
	}


	err2 := match.AddMatch(this.Services(), matchData)
	if err2 != nil {
		return this.RenderText(err2.Error())
	}

	return this.RenderJson(err2)
}
