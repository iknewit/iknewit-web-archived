// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

package controllers

import (
	"fmt"
	"github.com/revel/revel"
	cb "iknewit-web/app/controllers/api/base"
	"iknewit-web/app/services/achievement"
)

//** TYPES

type (
	// League controller the League api
	Achievements struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Achievements).Before, revel.BEFORE)
	revel.InterceptMethod((*Achievements).After, revel.AFTER)
	revel.InterceptMethod((*Achievements).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

func (this *Achievements) List() revel.Result {

	achievements, err := achievement.ListAchievements(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}
	fmt.Printf("%s\n", "achievements")

	return this.RenderJson(achievements)
}
