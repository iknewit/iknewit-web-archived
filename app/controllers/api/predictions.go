package controllers

import "github.com/revel/revel"

type Predictions struct {
	*revel.Controller
}

type Prediction struct {
	PredictionName string
}

func (c Predictions) Index() revel.Result {
	return c.Render()
}

func (c Predictions) List() revel.Result {
	predictions := []Prediction{
		Prediction{"World Cup"},
		Prediction{"European Championship"},
		Prediction{"English Premier League"},
		Prediction{"Champions League"},
		Prediction{"World Cup Qualifying - UEFA"},
		Prediction{"European Championship Qualifying"},
		Prediction{"International Friendly"},
		Prediction{"FA Cup"},
		Prediction{"Europa League"},
		Prediction{"Football League Championship"},
		Prediction{"Scottish Premier League"},
		Prediction{"Football League Play-offs"},
		Prediction{"Football League One"},
		Prediction{"Football League Two"},
		Prediction{"Football League Division Three North"},
		Prediction{"Football League Division Three South"},
		Prediction{"FA Community Shield"},
		Prediction{"Football League Cup"},
		Prediction{"Club World Cup"},
		Prediction{"Super Cup"},
		Prediction{"Football League Trophy"},
		Prediction{"Scottish Cup"},
		Prediction{"Scottish League Cup"},
		Prediction{"selected>Spanish La Liga"},
		Prediction{"Italian Serie A"},
		Prediction{"German Bundesliga"},
		Prediction{"French Ligue 1"},
	}

	return c.RenderJson(predictions)
}
