// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

package controllers

import (
	cb "iknewit-web/app/controllers/api/base"
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"iknewit-web/app/services/transaction"
	"iknewit-web/app/models/transaction"
	"io/ioutil"
)

//** TYPES

type (
	// League controller the League api
	Transactions struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Transactions).Before, revel.BEFORE)
	revel.InterceptMethod((*Transactions).After, revel.AFTER)
	revel.InterceptMethod((*Transactions).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

func (this *Transactions) Index() revel.Result {
	return this.Render()
}

//** CONTROLLER FUNCTIONS
func (this *Transactions) AddTransaction() revel.Result {
	//############## VerifyAccessToken

	userId, accessError := this.VerifyAccessToken()

	_ = userId
	if accessError != nil {
		return this.Forbidden("Forbidden")
	}

	//##############

	var transactionData transactionModel.Transaction

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &transactionData)
	if err != nil {
		return this.RenderText(err.Error())
	}


	err2 := transaction.AddTransaction(this.Services(), transactionData)
	if err2 != nil {
		return this.RenderText(err2.Error())
	}

	return this.RenderJson(err2)
}
