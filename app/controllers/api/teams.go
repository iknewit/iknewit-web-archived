// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	TeamController is implementing the Team specific api

*/
package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	cb "iknewit-web/app/controllers/api/base"
	"iknewit-web/app/models/team"
	"iknewit-web/app/services/competition"
	"iknewit-web/app/services/team"
	"io/ioutil"
)

//** TYPES

type (
	// Team controller the Team api
	Teams struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*Teams).Before, revel.BEFORE)
	revel.InterceptMethod((*Teams).After, revel.AFTER)
	revel.InterceptMethod((*Teams).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS
func (this *Teams) List() revel.Result {
	teams, err := team.FindAllTeams(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}

	return this.RenderJson(teams)
}

//** CONTROLLER FUNCTIONS
func (this *Teams) AddTeamForm() revel.Result {

	competitions, err := competition.FindAllCompetitions(this.Services())
	if err != nil {
		return this.RenderText(err.Error())
	}
	return this.Render(competitions)
}

//** CONTROLLER FUNCTIONS
func (this *Teams) AddTeam() revel.Result {

	var teamData teamModel.Team

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &teamData)
	if err != nil {
		return this.RenderText(err.Error())
	}

	err2 := team.AddTeam(this.Services(), teamData)
	if err2 != nil {
		return this.RenderText(err2.Error())
	}

	return this.RenderJson(err2)
}
