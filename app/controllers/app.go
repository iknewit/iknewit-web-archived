package controllers

import (
	cb "iknewit-web/app/controllers/api/base"
	//"code.google.com/p/go.crypto/bcrypt"
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"gopkg.in/mgo.v2"
	"iknewit-web/app/models/user"
	"iknewit-web/app/services/user"
	"io/ioutil"
)

type (
	// App controller controlls home page
	App struct {
		cb.BaseController
	}
)

//** INIT FUNCTION

// init is called when the first request into the controller is made
func init() {
	revel.InterceptMethod((*App).Before, revel.BEFORE)
	revel.InterceptMethod((*App).After, revel.AFTER)
	revel.InterceptMethod((*App).Panic, revel.PANIC)
}

//** CONTROLLER FUNCTIONS

// Index called to render the home page

func (this *App) Index() revel.Result {
	return this.Render()
}

/*
func (this *App) SignUp() revel.Result {
	return this.Render()
}
*/
func (this *App) SignUp() revel.Result {
	fmt.Printf("%s\n", this.Request.ContentType)

	var userData userModel.User

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &userData)

	if err != nil {
		return this.RenderText("Unmarshal Error : " + err.Error())
	}

	err2 := userService.SignUp(this.Services(), &userData)

	if err2 != nil {
		return this.Forbidden(err2.Error())
	}

	return this.RenderJson(userData)
}

func (this *App) SignIn() revel.Result {
	fmt.Printf("%s\n", this.Request.ContentType)
	var userData userModel.User

	content, _ := ioutil.ReadAll(this.Request.Body)
	fmt.Printf("%s\n", content)

	err := json.Unmarshal(content, &userData)

	if err != nil {
		return this.RenderText("Unmarshal Error : " + err.Error())
	}

	userProfile, err2 := userService.SignIn(this.Services(), userData)

	if err2 != nil {
		if err2 == mgo.ErrNotFound {
			return this.NotFound("You have either entered an incorrect email or password. Please try again. \xF0\x9F\x98\x89")
		}
		return this.Forbidden("Incorrect email or password. Please try again. \xF0\x9F\x98\x89")
	}

	return this.RenderJson(userProfile)
}

/*
func (this *App) VerifyToken() revel.Result {


    userId, err := this.VerifyAccessToken()

    if err != nil {
       return this.Forbidden("Forbidden")
    }

    return this.RenderText("Done")
}
*/
/*
func (this *App) SaveUser(user userModel.User, verifyPassword string) revel.Result {
    this.Validation.Required(verifyPassword)
	this.Validation.Required(verifyPassword == user.Password).
		Message("Password does not match")

	err := userService.SignUp(&user, this.Services())
	if err != nil {
		panic(err)
		return this.SignUp()
	}

	this.Session["user"] = user.Email
	this.Flash.Success("Welcome, " + user.DisplayName)
	return this.Redirect(routes.Leagues.Index())
}
*/
