package userService

import (
	"golang.org/x/crypto/bcrypt"
	"errors"
	"fmt"
	"gopkg.in/dgrijalva/jwt-go.v2"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/user"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"time"
)

const (
	SecretKey = "bmDdffFgfdshgsfdjwhfSAfmsladffsdehkeghfdgpf8dsfads"
)

// SignUp
func SignUp(service *services.Service, user *userModel.User) (err error) {
	defer helper.CatchPanic(&err, user.Email, "SignUp")

	tracelog.STARTED(user.Email, "SignUp")

	existUser := userModel.User{}

	fmt.Printf("Start registration - email :%s\n", user.Email)
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Find(bson.M{"Email": user.Email}).One(&existUser)
		})

	if err != nil {
		if err != mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignUp")
			return err
		}
	}

	// User Exist, Return User Exist
	if existUser.Email != "" {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignUp User Exists, "+existUser.Email)
		//panic("inconceivable")
		return errors.New("SignUp User Exists")
	}

	// Not Exist, Continue To Create New User
	// Hash User Password
	user.HashedPassword, _ = bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	user.Id = bson.NewObjectId()
	user.CreateTime = time.Now()

	var userPrediction userModel.UserPrediction
	userPrediction.Finished = 0
	userPrediction.Count = 0
	userPrediction.Win = 0
	userPrediction.Closeness = 0
	userPrediction.Accuracy = 0

	userPrediction.Type = "Score"
	user.Predictions = append(user.Predictions, userPrediction)
	userPrediction.Type = "HomeDrawAway"
	user.Predictions = append(user.Predictions, userPrediction)

	service.SendEmail(user.Email, user.Id.Hex())

	fmt.Printf("%s\n", user.Id)
	// Insert New User

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Insert(&user)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignUp")
		return err
	}

	// Create the token
	token := jwt.New(jwt.GetSigningMethod("HS256"))
	// Set some claims
	token.Claims["UserId"] = user.Id
	token.Claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString([]byte(SecretKey))
	user.HashedPassword = nil
	user.Password = ""
	user.AccessToken = tokenString

	fmt.Printf("%s\n", "SignUp End")

	tracelog.COMPLETED(user.Email, "SignUp")
	return err
}

func SignIn(service *services.Service, user userModel.User) (userProfile *userModel.User, err error) {
	defer helper.CatchPanic(&err, user.Email, "SignIn")

	tracelog.STARTED(user.Email, "SignIn")
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			//return err
			return collection.Find(bson.M{"Email": user.Email}).One(&userProfile)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignIn Failed - User not found")
			return nil, err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignIn Failed")
		return nil, err
	}

	userProfile.LastLoginTime = time.Now()

	if userProfile != nil {
		err = bcrypt.CompareHashAndPassword(userProfile.HashedPassword, []byte(user.Password))

		if err != nil {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignIn Failed - Username and Password Incorrect")
			return nil, err
		}

		change := bson.M{"$set": bson.M{"LastLoginTime": userProfile.LastLoginTime}}

		err = service.DBAction("user",
			func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": userProfile.Id}, change)
			})

		if err != nil {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateDisplayName")
			return nil, err
		}

		// SignIn Success

		// Create the token
		token := jwt.New(jwt.GetSigningMethod("HS256"))
		// Set some claims
		token.Claims["UserId"] = userProfile.Id
		token.Claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		// Sign and get the complete encoded token as a string
		tokenString, err := token.SignedString([]byte(SecretKey))

		tracelog.COMPLETED(userProfile.Email, "SignIn")
		userProfile.HashedPassword = nil
		userProfile.Password = ""
		userProfile.AccessToken = tokenString
		return userProfile, err

	} else {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignIn Failed - Username and Password Incorrect")
		return nil, err
	}

	tracelog.COMPLETED(user.Email, "SignIn")
	return nil, err
}

func ConfirmEmail(service *services.Service, userId string) (err error) {
	defer helper.CatchPanic(&err, userId,  "ConfirmEmail")

	tracelog.STARTED(userId, "ConfirmEmail")

	existUser := userModel.User{}
	fmt.Printf("Start Query - User Id : %s\n" , userId)
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&existUser)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ConfirmEmail- User not found")
			return err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ConfirmEmail")
		return err
	}

	change := bson.M{"$set": bson.M{"IsConfirmed": true}}

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {

			fmt.Printf("%s\n", "UpdateDisplayName "+userId)

			return collection.Update(bson.M{"_id": bson.ObjectIdHex(userId)}, change)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ConfirmEmail - User not found")
			return err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ConfirmEmail")
		return err
	}

	tracelog.COMPLETED(userId, "ConfirmEmail")
	return err
}

func UpdateDisplayName(service *services.Service, userId string, user userModel.User) (userProfile *userModel.User, err error) {
	defer helper.CatchPanic(&err, user.Email, "UpdateDisplayName")

	tracelog.STARTED(user.Email, "UpdateDisplayName")

	existUser := userModel.User{}
	_ = existUser
	fmt.Printf("%s\n", "Start Query")
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&userProfile)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateDisplayName- User not found")
			return nil, err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateDisplayName")
		return nil, err
	}

	change := bson.M{"$set": bson.M{"DisplayName": user.DisplayName}}

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {

			fmt.Printf("%s\n", "UpdateDisplayName "+userId)

			return collection.Update(bson.M{"_id": bson.ObjectIdHex(userId)}, change)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateDisplayName")
		return nil, err
	}

	fmt.Printf("%s\n", "UpdateDisplayName End")

	tracelog.COMPLETED(user.Email, "UpdateDisplayName")
	return nil, err
}

func GetUserProfile(service *services.Service, userId string) (userProfile *userModel.UserProfile, err error) {
	defer helper.CatchPanic(&err, userId, "GetUserProfile")

	tracelog.STARTED(userId, "GetUserProfile")

	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			//return err

			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&userProfile)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "GetUserProfile - User not found")
			return nil, err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "GetUserProfile Failed")
		return nil, err
	}

	if userProfile != nil {

		//userProfile.HashedPassword = nil
		//userProfile.Password = ""
		//userProfile.AccessToken = "";

	} else {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "SignIn Failed - Username and Password Incorrect")
		return nil, err
	}

	tracelog.COMPLETED(userId, "GetUserProfile")

	return userProfile, err
}

// UpdatePassword
func UpdatePassword(service *services.Service, userId string, userPassword *userModel.UserPassword) (err error) {
	defer helper.CatchPanic(&err, userId, "UpdatePassword")

	tracelog.STARTED(userId, "UpdatePassword")

	existUser := userModel.User{}
	_ = existUser
	fmt.Printf("%s\n", "Start Query")
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&existUser)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdatePassword - User not found")
			return err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdatePassword")
		return err
	}

	err = bcrypt.CompareHashAndPassword(existUser.HashedPassword, []byte(userPassword.CurrentPassword))

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdatePassword Failed - Username and Password Incorrect")
		return err
	}

	fmt.Printf("%s\n", userPassword.NewPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userPassword.NewPassword), bcrypt.DefaultCost)

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdatePassword Failed - Password Hash Error")
		return err
	}

	change := bson.M{"$set": bson.M{"HashedPassword": hashedPassword, "Password": userPassword.NewPassword}}

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {

			fmt.Printf("%s\n", "UpdatePassword "+userId)

			return collection.Update(bson.M{"_id": bson.ObjectIdHex(userId)}, change)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdatePassword")
		return err
	}

	fmt.Printf("%s\n", "UpdatePassword End")

	tracelog.COMPLETED(existUser.Email, "UpdatePassword")
	return err
}

func UpdateProfilePhoto(service *services.Service, userId, profilePhoto string) (userProfile *userModel.User, err error) {
	defer helper.CatchPanic(&err, userId, "UpdateProfilePhoto")

	tracelog.STARTED(userId, "UpdateProfilePhoto")

	existUser := userModel.User{}
	_ = existUser
	fmt.Printf("%s\n", "Start Query")
	// Check User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&userProfile)
		})

	if err != nil {
		if err == mgo.ErrNotFound {
			tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateProfilePhoto- User not found")
			return nil, err
		}

		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateProfilePhoto")
		return nil, err
	}

	lastUpdateTime := time.Now().Truncate(time.Millisecond)
	change := bson.M{"$set": bson.M{"ProfilePhoto": profilePhoto, "LastUpdateTime": lastUpdateTime}}

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {

			fmt.Printf("%s\n", "UpdateProfilePhoto "+userId)

			return collection.Update(bson.M{"_id": bson.ObjectIdHex(userId)}, change)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateProfilePhoto")
		return nil, err
	}

	userProfile.ProfilePhoto = profilePhoto
	userProfile.HashedPassword = nil
	userProfile.Password = ""
	userProfile.AccessToken = ""
	userProfile.LastUpdateTime = lastUpdateTime

	fmt.Printf("%s\n", "UpdateProfilePhoto End")

	tracelog.COMPLETED(userId, "UpdateProfilePhoto")
	return userProfile, err
}

func GetUserAchievement(service *services.Service, userId string) (UserAchievement *userModel.UserAchievement, err error) {
	defer helper.CatchPanic(&err, service.UserId, "GetUserAchievement")

	tracelog.STARTED(service.UserId, "GetMyAchievement")

	// Get User Exist or Not
	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			//return err

			return collection.Find(bson.M{"_id": bson.ObjectIdHex(userId)}).One(&UserAchievement)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "GetUserAchievement")
		return UserAchievement, err
	}

	return UserAchievement, err
}

func UpdateUserPredictions(service *services.Service, userId string, predictionType string, change interface{}) (err error) {
	defer helper.CatchPanic(&err, userId, "UpdateUserPredictions")

	tracelog.STARTED(userId, "UpdateUserPredictions")

	// Update user predictions count
	err = service.DBAction("user", func(collection *mgo.Collection) error {

		//change := bson.M{"$inc": bson.M{"Predictions.$.Count": 1}}

		return collection.Update(
			bson.M{"_id": bson.ObjectIdHex(userId),
				"Predictions": bson.M{
					"$elemMatch": bson.M{
						"Type": predictionType,
					},
				},
			},
			change,
		)
	})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "UpdateUserPredictions")
	}

	//userProfile.LastUpdateTime = lastUpdateTime

	fmt.Printf("%s\n", "UpdateUserPredictions End")

	tracelog.COMPLETED(userId, "UpdateUserPredictions")
	return err
}
