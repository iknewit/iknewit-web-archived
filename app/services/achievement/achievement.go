// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	league implements the service for the achievement functionality
*/
package achievement

import (
	"gopkg.in/mgo.v2"
	"iknewit-web/app/models/achievement"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
)

//** PUBLIC FUNCTIONS

func ListAchievements(service *services.Service) (achievements []*achievementModel.Achievement, err error) {
	defer helper.CatchPanic(&err, service.UserId, "ListArchievement")

	tracelog.STARTED(service.UserId, "ListArchievement")

	err = service.DBAction("achievement",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).All(&achievements)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ListArchievement")
		return achievements, err
	}

	//tracelog.COMPLETED(service.UserId, "ListArchievement")
	return achievements, err
}
