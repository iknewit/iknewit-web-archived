/*
	league implements the service for the match functionality
*/
package match

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/match"
	"iknewit-web/app/models/transaction"
	"iknewit-web/app/services"
	"iknewit-web/app/services/transaction"
	"iknewit-web/app/services/user"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"strings"
	"time"
)

//** PUBLIC FUNCTIONS

func Index(service *services.Service) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "FindAllMatches")

	tracelog.STARTED(service.UserId, "FindAllMatches")

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).Sort("KickoffAt", "CompetitionId", "Id").All(&matches)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "FindAllMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "FindAllMatches")
	return matches, err
}

// FindAllMatches retrieves the matches
func FindAllMatches(service *services.Service) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "FindAllMatches")

	tracelog.STARTED(service.UserId, "FindAllMatches")

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).Sort("KickoffAt", "CompetitionId", "Id").All(&matches)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "FindAllMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "FindAllMatches")
	return matches, err
}

func ListUpcomingMatches(service *services.Service, userId string, filterBy string, size, page int) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "ListUpcomingMatches")

	tracelog.STARTED(service.UserId, "ListUpcomingMatches")
	competitionIds := ParseCompetitionId(filterBy)

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {

			if len(competitionIds) > 0 {

				return collection.Find(bson.M{
					"CompetitionId": bson.M{"$in": competitionIds},
					"MatchStatus":   bson.M{"$in": []string{"Upcoming"}},
				}).Select(GetProjection(userId)).Sort("KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)

			}
			// End Refactor

			return collection.Find(bson.M{
				"MatchStatus": bson.M{"$in": []string{"Upcoming"}},
			}).Select(GetProjection(userId)).Sort("KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)

		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ListUpcomingMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "ListUpcomingMatches")
	return matches, err
}

func ListUserPredictionUpcomingMatches(service *services.Service, userId string, filterBy string, size, page int) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "ListMyPredictionUpcomingMatches")

	tracelog.STARTED(service.UserId, "ListMyPredictionUpcomingMatches")
	competitionIds := ParseCompetitionId(filterBy)

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			// Todo Start Refactor
			if len(competitionIds) > 0 {

				return collection.Find(bson.M{
					"CompetitionId": bson.M{"$in": competitionIds},
					"MatchStatus":   "Upcoming",
					"Predictions": bson.M{
						"$elemMatch": bson.M{
							"UserId": bson.ObjectIdHex(userId),
						},
					},
				}).Select(GetProjection(userId)).Sort("KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)

			}

			// End Refactor
			return collection.Find(bson.M{
				"MatchStatus": "Upcoming",
				"Predictions": bson.M{
					"$elemMatch": bson.M{
						"UserId": bson.ObjectIdHex(userId),
					},
				},
			}).Select(GetProjection(userId)).Sort("KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ListMyPredictionUpcomingMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "ListMyPredictionUpcomingMatches")
	return matches, err
}

func ListPastMatches(service *services.Service, userId, filterBy string, size, page int) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "ListPastMatches")

	tracelog.STARTED(service.UserId, "ListPastMatches")
	competitionIds := ParseCompetitionId(filterBy)

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {

			// Todo Start Refactor
			/*
						collection.Find(nil).Limit(size).All(&matches)
						fmt.Printf("Match ID : %s\n", matches[len(matches)-1].Id)
			/**/
			if len(competitionIds) > 0 {
				return collection.Find(bson.M{
					"CompetitionId": bson.M{"$in": competitionIds},
					"MatchStatus":   bson.M{"$in": []string{"Completed", "Ended"}}}).Select(GetProjection(userId)).Sort("-KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)
			}
			// End Refactor

			return collection.Find(bson.M{
				"MatchStatus": bson.M{"$in": []string{"Completed", "Ended"}},
			}).Select(GetProjection(userId)).Sort("-KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)

		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ListPastMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "ListPastMatches")
	return matches, err
}

func ListUserPredictionPastMatches(service *services.Service, userId string, filterBy string, size, page int) (matches []*matchModel.Match, err error) {
	defer helper.CatchPanic(&err, service.UserId, "ListUserPredictionPastMatches")

	tracelog.STARTED(service.UserId, "ListUserPredictionPastMatches")
	competitionIds := ParseCompetitionId(filterBy)

	// Capture the specified matches
	matches = []*matchModel.Match{}
	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			// Todo Start Refactor
			if len(competitionIds) > 0 {
				return collection.Find(bson.M{
					"CompetitionId": bson.M{"$in": competitionIds},
					"MatchStatus":   bson.M{"$in": []string{"Completed", "Ended"}},
					"Predictions": bson.M{
						"$elemMatch": bson.M{
							"UserId": bson.ObjectIdHex(userId),
						},
					},
				}).Select(GetProjection(userId)).Sort("-KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)
			}
			// End Refactor
			return collection.Find(bson.M{
				"MatchStatus": bson.M{"$in": []string{"Completed", "Ended"}},
				"Predictions": bson.M{
					"$elemMatch": bson.M{
						"UserId": bson.ObjectIdHex(userId),
					},
				},
			}).Select(GetProjection(userId)).Sort("-KickoffAt", "CompetitionId", "Id").Skip((page - 1) * size).Limit(size).All(&matches)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "ListUserPredictionPastMatches")
		return matches, err
	}

	tracelog.COMPLETED(service.UserId, "ListUserPredictionPastMatches")
	return matches, err
}

func Predict(service *services.Service, userId string, match matchModel.Match) (err error) {
	defer helper.CatchPanic(&err, service.UserId, "Predict Match")

	tracelog.STARTED(service.UserId, "Predict Match")

	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			return collection.Update(
				bson.M{"_id": match.Id, "KickoffAt": bson.M{"$gt": time.Now()}},
				bson.M{"$addToSet": bson.M{"Predictions": match.Predictions[0]}},
			)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "Predict Match")
		return err
	}

	//Add Transaction Record
	var transactionData transactionModel.Transaction

	transactionData.UserId = bson.ObjectIdHex(userId)
	if match.Predictions[0].PredictionType == "Score" {
		transactionData.Coins = 10
	} else {
		transactionData.Coins = 5
	}
	transactionData.Type = "Predict"
	transactionData.MatchId = match.Id

	err3 := transaction.AddTransaction(service, transactionData)
	if err3 != nil {
		return err3
	}

	change := bson.M{"$inc": bson.M{"Predictions.$.Count": 1}}

	err = userService.UpdateUserPredictions(service, userId, match.Predictions[0].PredictionType, change)

	tracelog.COMPLETED(service.UserId, "Predict Match")
	return err

}

func AddMatch(service *services.Service, match matchModel.Match) (err error) {
	defer helper.CatchPanic(&err, service.UserId, "Add Match")

	tracelog.STARTED(service.UserId, "Add Match")

	// Capture the specified predict

	match.Id = bson.NewObjectId()
	match.MatchStatus = "Upcoming"
	fmt.Println(match.KickoffAt)

	err = service.DBAction("match",
		func(collection *mgo.Collection) error {
			return collection.Insert(&match)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "Add Match")
		return err
	}

	tracelog.COMPLETED(service.UserId, "Add Match")
	return err

}

func GetProjection(userId string) (projection interface{}) {
	return bson.M{"HomeTeamId": 1, "AwayTeamId": 1, "HomeTeamName": 1, "AwayTeamName": 1, "KickoffAt": 1, "CompetitionId": 1, "CompetitionName": 1, "HomeTeamScore": 1,
		"AwayTeamScore": 1, "MatchStatus": 1, "Predictions": bson.M{"$elemMatch": bson.M{"UserId": bson.ObjectIdHex(userId)}}}
}

func ParseCompetitionId(filterBy string) (competitionIds []bson.ObjectId) {
	competitionIds = make([]bson.ObjectId, 0)
	fmt.Printf(filterBy)

	if filterBy != "" {
		filterByCompetitionId := strings.Split(filterBy, ",")
		competitionIds = make([]bson.ObjectId, len(filterByCompetitionId))
		for i := range filterByCompetitionId {
			competitionIds[i] = bson.ObjectIdHex(filterByCompetitionId[i])
		}
	}

	return competitionIds
}
