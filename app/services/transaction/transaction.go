// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	league implements the service for the match functionality
*/
package transaction

import (
	"gopkg.in/mgo.v2"
	"iknewit-web/app/models/transaction"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"gopkg.in/mgo.v2/bson"
	"fmt"
	"time"
)

//** PUBLIC FUNCTIONS

// Add Transaction
func AddTransaction(service *services.Service, transactionData transactionModel.Transaction) (err error) {
	defer helper.CatchPanic(&err, service.UserId, "Add Transaction")

	tracelog.STARTED(service.UserId, "Add Transaction")

	// Capture the specified predict

	transactionData.CreateTime = time.Now().Truncate(time.Millisecond)

	err = service.DBAction("transaction",
		func(collection *mgo.Collection) error {
			return collection.Insert(&transactionData)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "Add Transaction")
		return err
	}
	tracelog.COMPLETED(service.UserId, "Add Transaction")

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			fmt.Printf("%s, %s\n", transactionData.UserId, "Update User Record##########################################")

			change := bson.M{"$inc": bson.M{"Coins": transactionData.Coins, "PowerPoint": transactionData.PowerPoint}}

			return collection.Update(
				bson.M{"_id": transactionData.UserId},
				change,
			)
		})
	tracelog.COMPLETED(service.UserId, "Update User Record")

	return err

}
