// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	league implements the service for the league functionality
*/
package competition

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/competition"
	"iknewit-web/app/models/user"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"time"
)

//** PUBLIC FUNCTIONS

// FindRegion retrieves the stations for the specified region
func FindAllCompetitions(service *services.Service) (Competitions []*competitionModel.Competition, err error) {
	defer helper.CatchPanic(&err, service.UserId, "FindAllCompetitions")

	tracelog.STARTED(service.UserId, "FindAllCompetitions")

	// Capture the specified competition
	Competitions = []*competitionModel.Competition{}
	err = service.DBAction("competition",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).All(&Competitions)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "FindAllCompetitions")
		return Competitions, err
	}

	tracelog.COMPLETED(service.UserId, "FindAllCompetitions")
	return Competitions, err
}

func CreateCompetition(service *services.Service, userId string, competitionData competitionModel.Competition) (err error) {

	tracelog.STARTED(service.UserId, "CreateCompetition")

	fmt.Printf("%s\n", userId)

	competitionData.Id = bson.NewObjectId()
	//competitionData.CreateBy = bson.ObjectIdHex(userId)
	competitionData.IsPrivate = true
	competitionData.CreateTime = time.Now().Truncate(time.Millisecond)

	err = service.DBAction("competition",
		func(collection *mgo.Collection) error {
			return collection.Insert(&competitionData)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "CreateCompetition")
		return err
	}
	return err
}

func JoinCompetition(service *services.Service, userId string, competitionData competitionModel.CompetitionShareCode) (err error) {

	fmt.Printf("JoinCompetition \n")
	tracelog.STARTED(service.UserId, "CreateCompetition")

	// Capture the specified competition
	var Competitions = competitionModel.Competition{}

	// Check User Exist or Not
	err = service.DBAction("competition",
		func(collection *mgo.Collection) error {
			//return err

			return collection.Find(bson.M{"ShareCode": competitionData.ShareCode}).One(&Competitions)
		})

	fmt.Printf("Competition Found, Adding to user %s\n", userId)

	joinCompetition := userModel.UserCompetition{}
	joinCompetition.CompetitionId = Competitions.Id
	joinCompetition.IsBlocked = false

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			return collection.Update(
				bson.M{"_id": bson.ObjectIdHex(userId)},
				bson.M{"$addToSet": bson.M{"Competitions": joinCompetition}},
			)
		})

	fmt.Printf("Member Object: %s\n", joinCompetition)

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "JoinCompetition")
		return err
	}

	fmt.Printf("Join successfully \n")
	return nil
}
