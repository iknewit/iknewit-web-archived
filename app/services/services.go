// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	Services provides boilerplate functionality for all services.
	Any state required by all the services is maintained here.
*/
package services

import (
	"fmt"
	"github.com/revel/revel"
	"github.com/sendgrid/sendgrid-go"
	"gopkg.in/mgo.v2"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/mongo"
)

const sendGridAPIKey = "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"

//** TYPES

type (
	// Services contains common fields and behavior for all services
	Service struct {
		MongoSession *mgo.Session
		UserId       string
	}
)

//** PUBLIC FUNCTIONS

// DBAction executes queries and commands against MongoDB

func (this *Service) DBAction(collectionName string, mongoCall mongo.MongoCall) (err error) {

	return mongo.Execute(this.UserId, this.MongoSession, helper.MONGO_DATABASE, collectionName, mongoCall)
}

func (this *Service) SendEmail(email string, userId string) (err error) {
	sg := sendgrid.NewSendGridClientWithApiKey(sendGridAPIKey)
	message := sendgrid.NewMail()
	message.AddTo(email)
	//message.AddToName("Yamil Asusta")
	message.SetSubject("Confirm your email address and say iKnewit")
	message.SetText("SayIKnewIt Confirmation Email")

	const emailTemplate = `<html>
		<body>
			Hi there,
			<br/>
			Thanks for signing up. To get ready to say iKnewit, we just need you to confirm your email address below in 30 days:
			<a href="http://%s/api/v1/users/%s/confirmemail" title="Confirm Your Email">Confirm Your Email</a>
			<br/>
			Cheers,
			The iKnewit Team
		</body>
		</html>
	`
	emailBody := fmt.Sprintf(emailTemplate, revel.Config.StringDefault("iknewit.domain", ""), userId)

	message.SetHTML(emailBody)
	message.SetFrom("hello@sayiknewit.com")
	if r := sg.Send(message); r == nil {
		fmt.Println("Email : %s sent!", email)
	} else {
		fmt.Println(r)
	}
	return nil
}
