package integration

import (
	"fmt"
	"strconv"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/utilities/mongo"
	"github.com/revel/revel"
	"iknewit-web/app/models/api"
	"iknewit-web/app/models/match"
	"iknewit-web/app/models/team"
	"iknewit-web/app/models/competition"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"time"
	"encoding/json"
	"net/url"
	"net/http"
	"os"
 	"io/ioutil"

	//"math"
)

var footballAPIKey string
var footballAPIURL string

type IntegrationCompetition struct {
	// filtered
}


func (e IntegrationCompetition) Run() {
	// Queries the DB
	// Sends some email
	footballAPIKey = revel.Config.StringDefault("football.apikey", "")
	footballAPIURL = "http://football-api.com/api/?"
	RunIntegrationCompetition()
	fmt.Printf("%s\n", time.Now())
}

//** PUBLIC FUNCTIONS
// FindRegion retrieves the stations for the specified region
func RunIntegrationCompetition() (err error) {
	defer helper.CatchPanic(&err, "", "IntegrationCompetition Starting")
	tracelog.STARTED("", "Start IntegrationCompetition")
	fmt.Printf("%s\n",  "Start IntegrationCompetition")

	os.Setenv("HTTP_PROXY", os.Getenv("FIXIE_URL"))
 	os.Setenv("HTTPS_PROXY", os.Getenv("FIXIE_URL"))

	resp, err := http.PostForm(footballAPIURL, url.Values{"Action": {"competitions"}, "APIKey": {footballAPIKey}})
	if err != nil {
		 fmt.Printf("Request error : [%s]\n", err.Error())
		 return err
	}

	defer resp.Body.Close()
	
	var competitions APICompetitionModel.Competitions 
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		 fmt.Printf("Read all Error : [%s]\n", err.Error())
	}

	fmt.Printf("jsonDataFromHttp Result : [%s]\n", jsonDataFromHttp)

 	err = json.Unmarshal([]byte(jsonDataFromHttp), &competitions)
	if err != nil {
		 fmt.Printf("Unmarshal Error : [%s]\n", err.Error())
	} else {
		 fmt.Printf("API Result : [%s]\n", competitions)
	}

	for index, each := range competitions.Competition {
		fmt.Printf("[%d] is [%s]\n", index, each.Name)
		GetCompetitionStandings(each.Id)
		//GetCompetitionUpcomingMatches(each.Id)

	 	toDate := time.Now()
	 	fromDate := toDate.AddDate(0,0,-7)
	 	fmt.Printf("Get Past Competition Fixtures\n", index, each.Name)
		GetCompetitionFixtures(each.Id, fromDate, toDate)

	 	fromDate = time.Now()
	 	toDate = fromDate.AddDate(0,0,7)
	 	fmt.Printf("Get Future Competition Fixtures\n", index, each.Name)
		GetCompetitionFixtures(each.Id, fromDate, toDate)


		//GetLegacyCompetitionFixtures(each.Id)
	}

	fmt.Printf("%s\n",  "IntegrationCompetition Ended")
	
	return nil
}

func GetCompetitionStandings(competitionId string) (err error) {
	defer helper.CatchPanic(&err, "", "IntegrationCompetition")
	tracelog.STARTED("", "IntegrationCompetition")
	fmt.Printf("%s\n",  "IntegrationCompetition")

	os.Setenv("HTTP_PROXY", os.Getenv("FIXIE_URL"))
 	os.Setenv("HTTPS_PROXY", os.Getenv("FIXIE_URL"))
	resp, err := http.PostForm(footballAPIURL, url.Values{"Action": {"standings"}, "APIKey": {footballAPIKey}, "comp_id" : {competitionId}})
	
	defer resp.Body.Close()

	var standings APICompetitionModel.Standings 
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)

	fmt.Printf("jsonDataFromHttp Result : [%s]\n", jsonDataFromHttp)

	if err != nil {
		 fmt.Printf("Read all Error : [%s]\n", err.Error())
	}

 	err = json.Unmarshal([]byte(jsonDataFromHttp), &standings)
	if err != nil {
		 fmt.Printf("Unmarshal Error : [%s]\n", err.Error())
	}

	for index, each := range standings.Teams {
		fmt.Printf("[%d] is [%s]\n", index, each.StandTeamName)
	}
	
	return nil
}


func GetCompetitionUpcomingMatches(competitionId string) (err error) {
	defer helper.CatchPanic(&err, "", "GetCompetitionUpcomingMatches")
	tracelog.STARTED("", "GetCompetitionUpcomingMatches")
	fmt.Printf("%s\n",  "GetCompetitionUpcomingMatches")

	os.Setenv("HTTP_PROXY", os.Getenv("FIXIE_URL"))
 	os.Setenv("HTTPS_PROXY", os.Getenv("FIXIE_URL"))
	resp, err := http.PostForm(footballAPIURL, url.Values{"Action": {"today"}, "APIKey": {footballAPIKey}, "comp_id" : {competitionId}})
	
	defer resp.Body.Close()
	
	var match APICompetitionModel.Match 
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)
		
	fmt.Printf("jsonDataFromHttp Result : [%s]\n", jsonDataFromHttp)

	if err != nil {
		 fmt.Printf("Read all Error : [%s]\n", err.Error())
	}

 	err = json.Unmarshal([]byte(jsonDataFromHttp), &match)
	if err != nil {
		 fmt.Printf("Unmarshal Error : [%s]\n", err.Error())
	}

	for index, each := range match.Matches {
		fmt.Printf("[%d] is [%s] vs [%s]\n", index, each.MatchLocalteamName, each.MatchVisitorteamName)
	}
	
	return nil
}

func GetCompetitionFixtures(competitionId string, fromDate time.Time, toDate time.Time) (err error) {
	defer helper.CatchPanic(&err, "", "GetCompetitionFixtures")
	tracelog.STARTED("", "GetCompetitionFixtures")
	fmt.Printf("%s\n",  "GetCompetitionFixtures")


 	fromDateString := fmt.Sprintf("%02d.%02d.%d",fromDate.Day(), fromDate.Month(), fromDate.Year())
 	toDateString :=  fmt.Sprintf("%02d.%02d.%d",toDate.Day(), toDate.Month(), toDate.Year())

	os.Setenv("HTTP_PROXY", os.Getenv("FIXIE_URL"))
 	os.Setenv("HTTPS_PROXY", os.Getenv("FIXIE_URL"))
	resp, err := http.PostForm(footballAPIURL, url.Values{"Action": {"fixtures"}, "APIKey": {footballAPIKey}, "comp_id" : {competitionId}, "from_date": {fromDateString}, "to_date":{toDateString}})
	
	defer resp.Body.Close()
	
	var apiMatch APICompetitionModel.Match
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)

	fmt.Printf("jsonDataFromHttp Result : [%s]\n", jsonDataFromHttp)


	if err != nil {
		 fmt.Printf("Read all Error : [%s]\n", err.Error())
	}

 	err = json.Unmarshal([]byte(jsonDataFromHttp), &apiMatch)
	if err != nil {
		 fmt.Printf("Unmarshal Error : [%s]\n", err.Error())
	}

	mongoSessionId := "integration"
	mongoSession, err := mongo.CopyMonotonicSession(mongoSessionId)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

	matches := []*matchModel.Match{}

	for _, each := range apiMatch.Matches {
		err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"MatchID": each.MatchID,
			}).Select(
				bson.M{
					"_id" : 1,
					"HomeTeamId":    1,
					"AwayTeamId":    1,
					"KickoffAt":     1,
					"CompetitionId": 1,
					"HomeTeamScore": 1,
					"AwayTeamScore": 1,
					"MatchStatus":   1,
					"Predictions":   1,
				}).All(&matches)
		})

		var matchId = bson.NewObjectId()
		var matchStatus = ""
		
		_ = matchId
		_ = matchStatus


		if len(matches) == 0 {
			fmt.Printf("Adding New Match - Date : [%s] [%s], Home [%s] vs Away [%s]\n", each.MatchFormattedDate, each.MatchTime, each.MatchLocalteamName, each.MatchVisitorteamName)

			localTeam := teamModel.Team{}
		
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "team", func(collection *mgo.Collection) error {
				return collection.Find(bson.M{
				"MappingTeamId": each.MatchLocalteamID,
				}).One(&localTeam)
			})

			if err != nil {
				tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "localTeams")
				return  err
			}

			vistorTeam := teamModel.Team{}
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "team", func(collection *mgo.Collection) error {
				return collection.Find(bson.M{
				"MappingTeamId": each.MatchVisitorteamID,
				}).One(&vistorTeam)
			})

			if err != nil {
				tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "vistorTeams")
				return  err
			}

			competition := competitionModel.Competition{}
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "competition", func(collection *mgo.Collection) error {
				return collection.Find(bson.M{
				"_id": localTeam.PrimaryCompetitionId,
				}).One(&competition)
			})

			if err != nil {
				tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "vistorTeams")
				return  err
			}
			//2015-12-06T16:00:00.000Z
			kickoffAt, err := time.Parse("02.01.2006 15:04", fmt.Sprintf("%s %s",each.MatchFormattedDate, each.MatchTime))
			
			if err != nil {
				panic(err)
			}
			//Add Transaction Record
			var matchData matchModel.Match

			matchData.Id = matchId
			matchData.HomeTeamId = localTeam.Id
			matchData.AwayTeamId = vistorTeam.Id
			matchData.HomeTeamName = localTeam.Name
			matchData.AwayTeamName = vistorTeam.Name
			matchData.KickoffAt = kickoffAt
			matchData.CompetitionId = competition.Id
			matchData.CompetitionName = competition.Name
			matchData.HomeTeamScore = 0
			matchData.AwayTeamScore = 0
			matchData.MatchStatus = "Upcoming"
			matchData.MatchID = each.MatchID
		
			matchId = matchData.Id
			matchStatus = matchData.MatchStatus
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Insert(&matchData)
			})
		
			fmt.Printf("Match Added\n")

		} else {
			matchId = matches[0].Id
			matchStatus = matches[0].MatchStatus
		}

		if each.MatchStatus == "FT" && matchStatus == "Upcoming" {
			fmt.Printf("Check Match - Date : [%s] [%s], Home [%s] vs Away [%s]\n", each.MatchFormattedDate, each.MatchTime, each.MatchLocalteamName, each.MatchVisitorteamName)

			i, err := strconv.ParseInt(each.MatchVisitorteamScore, 10, 32)
			if err != nil {
			    panic(err)
			}
			awayTeamScore := int32(i)

			j, err := strconv.ParseInt(each.MatchLocalteamScore, 10, 32)
			if err != nil {
			    panic(err)
			}
			
			homeTeamScore := int32(j)

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": matchId}, 
						bson.M{"$set": bson.M{"AwayTeamScore": awayTeamScore,  "HomeTeamScore": homeTeamScore ,"MatchStatus": "Ended"}})
				})
		} else if each.MatchStatus == "Postp." {
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": matchId}, 
						bson.M{"$set": bson.M{"MatchStatus": "Postponed"}})
				})
		}

	}

	return nil
}

func GetLegacyCompetitionFixtures(competitionId string) (err error) {
	defer helper.CatchPanic(&err, "", "GetLegacyCompetitionFixtures")
	tracelog.STARTED("", "GetLegacyCompetitionFixtures")
	fmt.Printf("%s\n",  "GetLegacyCompetitionFixtures")


 	fromDate := time.Now().AddDate(0,0,-11)
 	toDate := fromDate.AddDate(0,0,-7)

 	fromDateString := fmt.Sprintf("%02d.%02d.%d",fromDate.Day(), fromDate.Month(), fromDate.Year())
 	toDateString :=  fmt.Sprintf("%02d.%02d.%d",toDate.Day(), toDate.Month(), toDate.Year())

	os.Setenv("HTTP_PROXY", os.Getenv("FIXIE_URL"))
 	os.Setenv("HTTPS_PROXY", os.Getenv("FIXIE_URL"))
	resp, err := http.PostForm(footballAPIURL, url.Values{"Action": {"fixtures"}, "APIKey": {footballAPIKey}, "comp_id" : {competitionId}, "from_date": {fromDateString}, "to_date":{toDateString}})
	
	defer resp.Body.Close()
	
	var apiMatch APICompetitionModel.Match

	
	jsonDataFromHttp, err := ioutil.ReadAll(resp.Body)	

	fmt.Printf("GetLegacyCompetitionFixtures Result : [%s]\n", jsonDataFromHttp)
	

	if err != nil {
		 fmt.Printf("Read all Error : [%s]\n", err.Error())
	}

 	err = json.Unmarshal([]byte(jsonDataFromHttp), &apiMatch)
	if err != nil {
		 fmt.Printf("Unmarshal Error : [%s]\n", err.Error())
	}

	mongoSessionId := "integration"
	mongoSession, err := mongo.CopyMonotonicSession(mongoSessionId)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

	matches := []*matchModel.Match{}

	for _, each := range apiMatch.Matches {
		err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"MatchID": each.MatchStaticID,
			}).Select(
				bson.M{
					"_id" : 1,
					"HomeTeamId":    1,
					"AwayTeamId":    1,
					"KickoffAt":     1,
					"CompetitionId": 1,
					"HomeTeamScore": 1,
					"AwayTeamScore": 1,
					"MatchStatus":   1,
					"Predictions":   1,
				}).All(&matches)
		})

		var matchId = bson.NewObjectId()
		var matchStatus = ""
		
		_ = matchId
		_ = matchStatus

		if each.MatchStatus == "FT" && matchStatus == "Upcoming" {
			fmt.Printf("Check Match - Date : [%s] [%s], Home [%s] vs Away [%s]\n", each.MatchFormattedDate, each.MatchTime, each.MatchLocalteamName, each.MatchVisitorteamName)

			i, err := strconv.ParseInt(each.MatchVisitorteamScore, 10, 32)
			if err != nil {
			    panic(err)
			}
			awayTeamScore := int32(i)

			j, err := strconv.ParseInt(each.MatchLocalteamScore, 10, 32)
			if err != nil {
			    panic(err)
			}
			
			homeTeamScore := int32(j)

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": matchId}, 
						bson.M{"$set": bson.M{"AwayTeamScore": awayTeamScore,  "HomeTeamScore": homeTeamScore ,"MatchStatus": "Ended"}})
				})
		} else if each.MatchStatus == "Postp." {
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": matchId}, 
						bson.M{"$set": bson.M{"MatchStatus": "Postponed"}})
				})
		}

	}

	return nil
}
