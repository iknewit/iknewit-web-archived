package scoring

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/match"
	"iknewit-web/app/models/transaction"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/mongo"
	"iknewit-web/utilities/tracelog"
	"time"
	//"encoding/json"
	//"math"
)

type ScoreCalculation struct {
	// filtered
}

func (e ScoreCalculation) Run() {
	// Queries the DB
	// Sends some email
	CalculateMatchResultPrediction()
	fmt.Printf("%s\n", time.Now())
}

//** PUBLIC FUNCTIONS
// FindRegion retrieves the stations for the specified region
func CalculateMatchResultPrediction() (err error) {
	defer helper.CatchPanic(&err, "", "CalculateMatchResultPrediction")
	tracelog.STARTED("", "CalculateMatchResultPrediction")

	matches := []*matchModel.Match{}
	mongoSessionId := "ScheduledJob"

	mongoSession, err := mongo.CopyMonotonicSession(mongoSessionId)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

	err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"MatchStatus": "Ended",
		}).Select(
			bson.M{
				"HomeTeamId":    1,
				"AwayTeamId":    1,
				"KickoffAt":     1,
				"CompetitionId": 1,
				"HomeTeamScore": 1,
				"AwayTeamScore": 1,
				"MatchStatus":   1,
				"Predictions":   1,
			}).All(&matches)
	})

	if len(matches) != 0 {

		for i := 0; i < len(matches); i++ {
			match := matches[i]

			homeDrawAwayTotalCount := 0
			homeDrawAwayWinCount := 0
			scoreTotalCount := 0
			scoreMatchWinCount := 0
			scoreExactWinCount := 0

			// Match Result Prediction Correct
			for j := 0; j < len(match.Predictions); j++ {

				prediction := match.Predictions[j]
				prediction.Result = "Lose"

				var transactionData transactionModel.Transaction
				var message string

				if prediction.PredictionType == "HomeDrawAway" { // Match Result Prediction (Correct)
					homeDrawAwayTotalCount++

					if match.HomeTeamScore == match.AwayTeamScore && prediction.GameResult == "Draw" {
						prediction.Coins = 40
						prediction.PowerPoint = 10
						prediction.Result = "Win"

						transactionData.Type = "PredictHomeDrawAwayResult"
						homeDrawAwayWinCount++
					} else if match.HomeTeamScore > match.AwayTeamScore && prediction.GameResult == "Home" {
						prediction.Coins = 40
						prediction.PowerPoint = 10
						prediction.Result = "Win"

						transactionData.Type = "PredictHomeDrawAwayResult"
						homeDrawAwayWinCount++
					} else if match.HomeTeamScore < match.AwayTeamScore && prediction.GameResult == "Away" {
						prediction.Coins = 40
						prediction.PowerPoint = 10
						prediction.Result = "Win"

						transactionData.Type = "PredictHomeDrawAwayResult"
						homeDrawAwayWinCount++
					}

				} else if prediction.PredictionType == "Score" {
					scoreTotalCount++

					if (match.HomeTeamScore > match.AwayTeamScore && prediction.HomeTeamScore > prediction.AwayTeamScore) ||
						(match.HomeTeamScore < match.AwayTeamScore && prediction.HomeTeamScore < prediction.AwayTeamScore) {
						scoreMatchWinCount++
					}

					if match.HomeTeamScore == prediction.HomeTeamScore && match.AwayTeamScore == prediction.AwayTeamScore { // Exact Score Prediction (Correct Exact Score)
						fmt.Printf("Exact Score Prediction (Correct Exact Score)\n")

						message = "Exact Score Prediction (Correct Exact Score)"
						prediction.Coins = 200
						prediction.PowerPoint = 60
						prediction.Result = "Win"
						scoreExactWinCount++

						transactionData.Type = "PredictScoreResult"

					} else if (match.HomeTeamScore - match.AwayTeamScore) == (prediction.HomeTeamScore - prediction.AwayTeamScore) {
						fmt.Printf("Exact Score Prediction (Wrong Exact Score but Correct Match Win/Lose Result within same winning goal difference)\n")
						message = "Exact Score Prediction (Wrong Exact Score but Correct Match Win/Lose Result within same winning goal difference)"
						prediction.Coins = 80
						prediction.PowerPoint = 20
						prediction.Result = "Closeness"
						transactionData.Type = "PredictScoreResultCloseness"

					} else if (match.HomeTeamScore == match.AwayTeamScore) && (prediction.HomeTeamScore == prediction.AwayTeamScore) && ((match.HomeTeamScore+match.AwayTeamScore)+2 <= (prediction.HomeTeamScore+prediction.AwayTeamScore) ||
						(match.HomeTeamScore+match.AwayTeamScore)-2 <= (prediction.HomeTeamScore+prediction.AwayTeamScore)) { // Exact Score Prediction (Wrong Exact Score but Correct Match Draw Result within 2+/- total goals)

						fmt.Printf("Exact Score Prediction (Wrong Exact Score but Correct Match Draw Result within 2+/- total goals)\n")
						//2:2 (Total goal 4), 1:1 (Total goal 2), 3:3 (Total goal 6) 咁咪賽果嘅正負 2
						message = "Exact Score Prediction (Wrong Exact Score but Correct Match Draw Result within 2+/- total goals)"
						prediction.Coins = 80
						prediction.PowerPoint = 20
						prediction.Result = "Closeness"
						transactionData.Type = "PredictScoreResultCloseness"
					} else if ((match.HomeTeamScore > match.AwayTeamScore) && (prediction.HomeTeamScore > prediction.AwayTeamScore)) ||
							((match.HomeTeamScore < match.AwayTeamScore) && (prediction.HomeTeamScore < prediction.AwayTeamScore)) ||
							((match.HomeTeamScore == match.AwayTeamScore) && (prediction.HomeTeamScore == prediction.AwayTeamScore)) {

							fmt.Printf("Exact Score Prediction (Wrong Exact Score but Other Correct Match Result)\n")

							message = "Exact Score Prediction (Wrong Exact Score but Other Correct Match Result)"
							prediction.Coins = 40
							prediction.PowerPoint = 10
							prediction.Result = "Closeness"
							transactionData.Type = "PredictScoreResultCloseness"
					}
				}

				if prediction.Result == "Win" || prediction.Result == "Closeness" {
					fmt.Printf("Insert Transaction User : %s, %s %s\n", prediction.UserId, message, " Record")

					transactionData.UserId = prediction.UserId
					transactionData.Coins = prediction.Coins
					transactionData.PowerPoint = prediction.PowerPoint
					transactionData.MatchId = match.Id
					transactionData.KickoffAt = match.KickoffAt
					transactionData.CreateTime = time.Now().Truncate(time.Millisecond)

					err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "transaction", func(collection *mgo.Collection) error {
						return collection.Insert(&transactionData)
					})
				}

				// Update User Prediction in Match
				err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
					fmt.Printf("Update Match User : %s, %s\n", prediction.UserId, " Record")

					change := bson.M{"$set": bson.M{"Predictions.$.Coins": prediction.Coins, "Predictions.$.PowerPoint": prediction.PowerPoint, "Predictions.$.Result": prediction.Result}}

					return collection.Update(
						bson.M{"_id": match.Id,
							"Predictions": bson.M{
								"$elemMatch": bson.M{
									"UserId": prediction.UserId,
								},
							},
						},
						change,
					)
				})

				// Update User Profile Coins, PowerPoint and Win When User Win the Match
				if prediction.Result == "Win" {
					err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
						fmt.Printf("Update User : %s, Prediction Type : %s\n", prediction.UserId, prediction.PredictionType)

						change := bson.M{"$inc": bson.M{"Coins": prediction.Coins, "PowerPoint": prediction.PowerPoint, "Predictions.$.Win": 1, "Predictions.$.Finished": 1}}

						return collection.Update(
							bson.M{"_id": prediction.UserId,
								"Predictions": bson.M{
									"$elemMatch": bson.M{
										"Type": prediction.PredictionType,
									},
								},
							},
							change,
						)
					})
				} else if prediction.Result == "Closeness" {
					err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
						fmt.Printf("Update User : %s, Prediction Type : %s\n", prediction.UserId, prediction.PredictionType)

						change := bson.M{"$inc": bson.M{"Coins": prediction.Coins, "PowerPoint": prediction.PowerPoint, "Predictions.$.Closeness": 1, "Predictions.$.Finished": 1}}

						return collection.Update(
							bson.M{"_id": prediction.UserId,
								"Predictions": bson.M{
									"$elemMatch": bson.M{
										"Type": prediction.PredictionType,
									},
								},
							},
							change,
						)
					})
				} else if prediction.Result == "Lose" {
					err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
						fmt.Printf("Update User : %s, Prediction Type : %s\n", prediction.UserId, prediction.PredictionType)

						change := bson.M{"$inc": bson.M{"Predictions.$.Finished": 1}}

						return collection.Update(
							bson.M{"_id": prediction.UserId,
								"Predictions": bson.M{
									"$elemMatch": bson.M{
										"Type": prediction.PredictionType,
									},
								},
							},
							change,
						)
					})
				}
			}

			fmt.Printf("Match Id : %s\n", match.Id)
			fmt.Printf("==========================================================================================\n")
			fmt.Printf("Match Result Prediction (Correct Match Result Bonus - Less than 20%% are correct)\n")
			fmt.Printf("PredictHomeDrawAwayResultBonus - homeDrawAwayTotalCount: %d, homeDrawAwayWinCount : %d \n", homeDrawAwayTotalCount, homeDrawAwayWinCount)

			// Match Result Prediction (Correct Match Result Bonus - Less than 20% are correct)
			if float64(homeDrawAwayWinCount)/float64(homeDrawAwayTotalCount)*100 <= 20 {
				PredictHomeDrawAwayResultBonus(match, mongoSessionId, mongoSession)
			} else {
				fmt.Printf("%s\n", "No Bonus")
			}

			fmt.Printf("==========================================================================================\n")
			fmt.Printf("Exact Score Prediction (Correct Exact Score Bonus - Less than 5%% are correct)\n")
			fmt.Printf("PredictScoreResultBonus5 - scoreTotalCount: %d, scoreExactWinCount : %d \n", scoreTotalCount, scoreExactWinCount)

			if scoreExactWinCount > 0 && float64(scoreExactWinCount)/float64(scoreTotalCount)*100 <= 5 {
				PredictScoreResultBonus5(match, mongoSessionId, mongoSession)
			} else {
				fmt.Printf("%s\n", "No Bonus")
			}

			fmt.Printf("==========================================================================================\n")
			fmt.Printf("Exact Score Prediction (Correct Match Result Bonus - Less than 20%% are correct)\n")
			fmt.Printf("PredictScoreResultBonus20 - scoreTotalCount: %d, scoreMatchWinCount : %d \n", scoreTotalCount, scoreMatchWinCount)

			if scoreMatchWinCount > 0 && float64(scoreMatchWinCount)/float64(scoreTotalCount)*100 <= 20 {
				PredictScoreResultBonus20(match, mongoSessionId, mongoSession)
			} else {
				fmt.Printf("%s\n", "No Bonus")
			}

			//Update Match Status to Compelete
			fmt.Printf("==========================================================================================\n")
			fmt.Printf("%s\n", "Update March Status to Completed")
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": match.Id}, bson.M{"$set": bson.M{"MatchStatus": "Completed"}})
			})
		}

	} else {
		fmt.Printf("No match(es)")
	}

	mongo.CloseSession(mongoSessionId, mongoSession)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

	tracelog.COMPLETED("", "CalculateMatchResultPrediction")
	return nil
}

func PredictHomeDrawAwayResultBonus(match *matchModel.Match, mongoSessionId string, mongoSession *mgo.Session) {
	fmt.Printf("%s\n", "PredictHomeDrawAwayResultBonus =============================== ")

	bonusMatches := []*matchModel.Match{}
	err := mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"_id": match.Id,
		}).Select(
			bson.M{
				"HomeTeamId":    1,
				"AwayTeamId":    1,
				"KickoffAt":     1,
				"CompetitionId": 1,
				"HomeTeamScore": 1,
				"AwayTeamScore": 1,
				"MatchStatus":   1,
				"Predictions":   1,
			}).All(&bonusMatches)
	})

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
	}

	for j := 0; j < len(bonusMatches[0].Predictions); j++ {
		if bonusMatches[0].Predictions[j].Result == "Win" && bonusMatches[0].Predictions[j].PredictionType == "HomeDrawAway" {
			fmt.Printf("%s\n", bonusMatches[0].Predictions[j].UserId)

			//Add Transaction Record
			var transactionData transactionModel.Transaction

			transactionData.UserId = bonusMatches[0].Predictions[j].UserId
			transactionData.Coins = 40
			transactionData.PowerPoint = 10
			transactionData.Type = "PredictHomeDrawAwayResultBonus"
			transactionData.MatchId = match.Id
			transactionData.KickoffAt = match.KickoffAt
			transactionData.CreateTime = time.Now().Truncate(time.Millisecond)

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "transaction", func(collection *mgo.Collection) error {
				return collection.Insert(&transactionData)
			})

			fmt.Printf("Update User : %s, %s\n", transactionData.UserId, " Record ")

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": transactionData.UserId}, bson.M{"$inc": bson.M{"Coins": transactionData.Coins, "PowerPoint": transactionData.PowerPoint}})
			})

		}

	}
}

func PredictScoreResultBonus5(match *matchModel.Match, mongoSessionId string, mongoSession *mgo.Session) {
	fmt.Printf("%s\n", "PredictScoreResultBonus5 =============================== ")

	bonusMatches := []*matchModel.Match{}
	err := mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"_id": match.Id,
		}).Select(
			bson.M{
				"HomeTeamId":    1,
				"AwayTeamId":    1,
				"KickoffAt":     1,
				"CompetitionId": 1,
				"HomeTeamScore": 1,
				"AwayTeamScore": 1,
				"MatchStatus":   1,
				"Predictions":   1,
			}).All(&bonusMatches)
	})

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
	}

	for j := 0; j < len(bonusMatches[0].Predictions); j++ {

		if bonusMatches[0].Predictions[j].Result == "Win" && bonusMatches[0].Predictions[j].PredictionType == "Score" {
			fmt.Printf("%s\n", bonusMatches[0].Predictions[j].UserId)

			//Add Transaction Record
			var transactionData transactionModel.Transaction

			transactionData.UserId = bonusMatches[0].Predictions[j].UserId
			transactionData.Coins = 160
			transactionData.PowerPoint = 50
			transactionData.Type = "PredictScoreResultBonus5"
			transactionData.MatchId = match.Id
			transactionData.KickoffAt = match.KickoffAt
			transactionData.CreateTime = time.Now().Truncate(time.Millisecond)

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "transaction", func(collection *mgo.Collection) error {
				return collection.Insert(&transactionData)
			})

			fmt.Printf("Update User : %s, %s\n", transactionData.UserId, " Record ")
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {

				change := bson.M{"$inc": bson.M{"Coins": transactionData.Coins, "PowerPoint": transactionData.PowerPoint}}

				return collection.Update(
					bson.M{"_id": transactionData.UserId},
					change,
				)
			})

		}

	}
}

func PredictScoreResultBonus20(match *matchModel.Match, mongoSessionId string, mongoSession *mgo.Session) {
	fmt.Printf("%s\n", "PredictScoreResultBonus20 =============================== ")

	bonusMatches := []*matchModel.Match{}
	err := mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "match", func(collection *mgo.Collection) error {
		return collection.Find(bson.M{
			"_id": match.Id,
		}).Select(
			bson.M{
				"HomeTeamId":    1,
				"AwayTeamId":    1,
				"KickoffAt":     1,
				"CompetitionId": 1,
				"HomeTeamScore": 1,
				"AwayTeamScore": 1,
				"MatchStatus":   1,
				"Predictions":   1,
			}).All(&bonusMatches)
	})

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
	}

	for j := 0; j < len(bonusMatches[0].Predictions); j++ {

		if (match.HomeTeamScore == match.AwayTeamScore && bonusMatches[0].Predictions[j].HomeTeamScore == bonusMatches[0].Predictions[j].AwayTeamScore && bonusMatches[0].Predictions[j].PredictionType == "Score") ||
			(match.HomeTeamScore > match.AwayTeamScore && bonusMatches[0].Predictions[j].HomeTeamScore > bonusMatches[0].Predictions[j].AwayTeamScore && bonusMatches[0].Predictions[j].PredictionType == "Score") ||
			(match.HomeTeamScore < match.AwayTeamScore && bonusMatches[0].Predictions[j].HomeTeamScore < bonusMatches[0].Predictions[j].AwayTeamScore && bonusMatches[0].Predictions[j].PredictionType == "Score") {

			fmt.Printf("%s\n", bonusMatches[0].Predictions[j].UserId)

			//Add Transaction Record
			var transactionData transactionModel.Transaction

			transactionData.UserId = bonusMatches[0].Predictions[j].UserId
			transactionData.Coins = 40
			transactionData.PowerPoint = 10
			transactionData.Type = "PredictScoreResultBonus20"
			transactionData.MatchId = match.Id
			transactionData.KickoffAt = match.KickoffAt
			transactionData.CreateTime = time.Now().Truncate(time.Millisecond)

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "transaction", func(collection *mgo.Collection) error {
				return collection.Insert(&transactionData)
			})
			fmt.Printf("Update User : %s, %s\n", transactionData.UserId, " Record ")

			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
				return collection.Update(bson.M{"_id": transactionData.UserId}, bson.M{"$inc": bson.M{"Coins": transactionData.Coins, "PowerPoint": transactionData.PowerPoint}})
			})

		}

	}
}
