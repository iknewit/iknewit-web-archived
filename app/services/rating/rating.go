package rating

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/rating"
	//"iknewit-web/app/models/user"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/mongo"
	"iknewit-web/utilities/tracelog"
	"time"
	//"encoding/json"
	//"math"
)

type RatingCalculation struct {
	// filtered
}

func (e RatingCalculation) Run() {
	// Queries the DB
	fmt.Printf("Start Ranking Calculation %s\n", time.Now())
	RatingResult()
	fmt.Printf("End Ranking Calculation %s\n", time.Now())

}

//** PUBLIC FUNCTIONS
// FindRegion retrieves the stations for the specified region
func RatingResult() (err error) {
	defer helper.CatchPanic(&err, "", "RatingResult")
	tracelog.STARTED("", "RatingResult")

	ratings := []*ratingModel.Rating{}
	mongoSessionId := "ScheduledJob-Rating"

	mongoSession, err := mongo.CopyMonotonicSession(mongoSessionId)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

   t := time.Now();

	dateStart := time.Date(t.Year(), t.Month() -1, t.Day(), 0, 0, 0, 0, time.UTC);
	err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "transaction", func(collection *mgo.Collection) error {
		o1 := bson.M{
			"$match": bson.M{"KickoffAt": bson.M{"$gt": dateStart}},
		}
		o2 := bson.M{
			"$group": bson.M{
				"_id": "$UserId",
				"TotalScore": bson.M{
					"$sum": "$PowerPoint",
				},
			},
		}
		o3 := bson.M{
			"$sort": bson.M{
				"TotalScore": -1,
			},
		}

		operations := []bson.M{o1, o2, o3}
		pipe := collection.Pipe(operations)

		return pipe.All(&ratings)
	})

	if err != nil {
		fmt.Printf("ERROR : %s\n", err.Error())
		return
	}

fmt.Printf("Ranking Calculation In Progress %d\n", len(ratings))
	if len(ratings) != 0 {
		rank := 0
		prevScore := 0


		// Pre-set User Rank
		err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
			change := bson.M{"$set": bson.M{"Rank": rank}}
			recCnt, err2 := collection.UpdateAll( bson.M{"Rank": bson.M{"$gt": 0}} , change)
			fmt.Printf("Ranking Calculation Updated record count %d\n", recCnt)
			return err2
		})


		for i := 0; i < len(ratings); i++ {
			if i == 0 {
				rank = 1
				prevScore = ratings[i].TotalScore
			}
			if prevScore > ratings[i].TotalScore {
				rank = i+1
				prevScore = ratings[i].TotalScore
			}

			fmt.Printf("Updating Rank %d\n", rank)
			// Update User Prediction in Match
			err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
				change := bson.M{"$set": bson.M{"Rank": rank}}

				return collection.Update(bson.M{"_id": ratings[i].Id}, change)

			})

		}


		// Pre-set User Rank
		err = mongo.Execute(mongoSessionId, mongoSession, helper.MONGO_DATABASE, "user", func(collection *mgo.Collection) error {
			change := bson.M{"$set": bson.M{"Rank": rank+1}}
			recCnt, err2 := collection.UpdateAll( bson.M{"Rank": bson.M{"$eq": 0}} , change)
			fmt.Printf("Ranking Calculation Updated record count %d\n", recCnt)
			return err2
		})



	}

	mongo.CloseSession(mongoSessionId, mongoSession)

	if err != nil {
		tracelog.ERRORf(err, mongoSessionId, "Before", "")
		return err
	}

	tracelog.COMPLETED("", "RankingResult")
	return nil
}
