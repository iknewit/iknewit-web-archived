// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	team implements the service for the team functionality
*/
package team

import (
	"gopkg.in/mgo.v2"
	"iknewit-web/app/models/team"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
	"gopkg.in/mgo.v2/bson"
)

//** PUBLIC FUNCTIONS

// FindRegion retrieves the stations for the specified region
func FindAllTeams(service *services.Service) (teams []*teamModel.Team, err error) {
	defer helper.CatchPanic(&err, service.UserId, "FindAllTeams")

	tracelog.STARTED(service.UserId, "FindAllTeams")

	// Capture the specified team
	teams = []*teamModel.Team{}
	err = service.DBAction("team",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).All(&teams)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "FindAllTeams")
		return teams, err
	}

	tracelog.COMPLETED(service.UserId, "FindAllTeams")
	return teams, err
}

// Add Team
func AddTeam(service *services.Service, team teamModel.Team) (err error) {
	defer helper.CatchPanic(&err, service.UserId, "Add Team")

	tracelog.STARTED(service.UserId, "Add Team")

	// Capture the specified predict
    
	team.Id = bson.NewObjectId()
	//team.Name = "Athletic Bilbao"
    //team.Abbrev = "ATH"
    //team.PrimaryCompetitionId = bson.ObjectIdHex("54d46c930c6c526ac8e91646") 
    //team.Slug = "athletic-bilbao"
    
	err = service.DBAction("team",
		func(collection *mgo.Collection) error {
			return collection.Insert(&team)
		})
    
    
            /*
            
            
            type Team struct {
                Id                   bson.ObjectId `bson:"_id,omitempty"`
                Name                 string        `bson:"Name" json:"Name"`
                Abbrev               string        `bson:"Abbrev" json:"Abbrev"`
                PrimaryCompetitionId bson.ObjectId `bson:"PrimaryCompetitionId,omitempty"`
                Slug                 string        `bson:"Slug" json:"Slug"`
                CrestPathPrefix      string        `bson:"CrestPathPrefix" json:"CrestPathPrefix"`
                CrestSmallPath       string        `bson:"CrestSmallPath" json:"CrestSmallPath"`
                CrestMediumPath      string        `bson:"CrestMediumPath" json:"CrestMediumPath"`
            }

            {
                "_id" : ObjectId("54e01beb4e7f3b28c96f1bf5"),
                "Name" : "Arsenal",
                "Abbrev" : "ARS",
                "PrimaryCompetitionId" : ObjectId("54d46c930c6c526ac8e91631"),
                "Slug" : "arsenal",
                "CrestPathPrefix" : "/static_assets/teams/crests/arsenal",
                "CrestSmallPath" : "/static_assets/teams/crests/arsenal_small@2x.png",
                "CrestMediumPath" : "/static_assets/teams/crests/arsenal_medium@2x.png"
            }
            
            return collection.Update(
                bson.M{ "_id": match.Id },
                bson.M{ "$addToSet" : bson.M{ "Predictions":  match.Predictions[0]}},
            )
            //bson.M{ "UserId" : bson.ObjectIdHex("54e08d514e7f3b28c96f1c81"), "PredictionType" : "Score", "HomeTeamScore" : 1, "AwayTeamScore" : 1, "GameResult" : "123", "Result" : "Closeness", "PowerPoint" : 10, "Coins" : 50}
		})
*/
    
	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "Add Team")
		return err
	}
    
	tracelog.COMPLETED(service.UserId, "Add Team")
	return err
    
}