// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	league implements the service for the leaderboard functionality
*/
package leaderboard

import (
	"gopkg.in/mgo.v2"
	"iknewit-web/app/models/leaderboard"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	"iknewit-web/utilities/tracelog"
)

//** PUBLIC FUNCTIONS

func ListLeaderboard(service *services.Service, size, page int) (leaderboard []*leaderboardModel.Leaderboard, err error) {
	defer helper.CatchPanic(&err, service.UserId, "LeaderboardList")

	tracelog.STARTED(service.UserId, "LeaderboardList")

	err = service.DBAction("user",
		func(collection *mgo.Collection) error {
			//return collection.Find(nil).Sort("-Coins","-PowerPoint", "Rank").Skip((page - 1) * size).Limit(size).All(&leaderboard)
			return collection.Find(nil).Sort("-Coins","-PowerPoint", "Rank").All(&leaderboard)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "LeaderboardList")
		return leaderboard, err
	}

	//tracelog.COMPLETED(service.UserId, "LeaderboardList")
	return leaderboard, err
}
