// Copyright 2013 Ardan Studios. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE handle.

/*
	league implements the service for the league functionality
*/
package league

import (
	"iknewit-web/app/models/league"
	"iknewit-web/app/services"
	"iknewit-web/utilities/helper"
	//"iknewit-web/utilities/mongo"
	"gopkg.in/mgo.v2"
	"iknewit-web/utilities/tracelog"
	//"gopkg.in/mgo.v2/bson"
)

//** PUBLIC FUNCTIONS

// FindRegion retrieves the stations for the specified region
func FindAllLeagues(service *services.Service) (leagues []*leagueModel.League, err error) {
	defer helper.CatchPanic(&err, service.UserId, "FindAllLeagues")

	tracelog.STARTED(service.UserId, "FindAllleagues")

	// Capture the specified League
	leagues = []*leagueModel.League{}
	err = service.DBAction("league",
		func(collection *mgo.Collection) error {
			return collection.Find(nil).All(&leagues)
		})

	if err != nil {
		tracelog.COMPLETED_ERROR(err, helper.MAIN_GO_ROUTINE, "FindAllLeagues")
		return leagues, err
	}

	tracelog.COMPLETED(service.UserId, "FindAllLeagues")
	return leagues, err
}
