package APICompetitionModel


type 	(
  Competitions struct {
		APIVersion                int           `bson:"APIVersion" json:"APIVersion"`
    APIRequestsRemaining      int           `bson:"APIRequestsRemaining" json:"APIRequestsRemaining"`
    DeveloperAuthentication   string        `bson:"DeveloperAuthentication" json:"DeveloperAuthentication"`
    Competition  []struct {
        Id        string                    `bson:"id" json:"id"`
        Name      string                    `bson:"name" json:"name"`
        Region    string                    `bson:"region" json:"region"`
      }                                     `bson:"Competition" json:"Competition"`
    Action                    string        `bson:"Action" json:"Action"`
    Params  struct {
        Action    string                    `bson:"Action" json:"Action"`
        APIKey    string                    `bson:"APIKey" json:"APIKey"`
      }                                     `bson:"Params" json:"Params"`
    ComputationTime           float64       `bson:"ComputationTimestring" json:"ComputationTimestring"`
    IP                        string        `bson:"IP" json:"IP"`
    ERROR                     string        `bson:"ERROR" json:"ERROR"`
    ServerName                string        `bson:"ServerName" json:"ServerName"`
    ServerAddress             string        `bson:"ServerAddress" json:"ServerAddress"`

	}

  Standings struct {
  APIRequestsRemaining    int     `json:"APIRequestsRemaining"`
  APIVersion              int     `json:"APIVersion"`
  Action                  string  `json:"Action"`
  ComputationTime         float64 `json:"ComputationTime"`
  DeveloperAuthentication string  `json:"DeveloperAuthentication"`
  ERROR                   string  `json:"ERROR"`
  IP                      string  `json:"IP"`
  Params                  struct {
    APIKey string `json:"APIKey"`
    Action string `json:"Action"`
    CompID string `json:"comp_id"`
  } `json:"Params"`
  ServerAddress string `json:"ServerAddress"`
  ServerName    string `json:"ServerName"`
  Teams         []struct {
    StandAwayD         string `json:"stand_away_d"`
    StandAwayGa        string `json:"stand_away_ga"`
    StandAwayGp        string `json:"stand_away_gp"`
    StandAwayGs        string `json:"stand_away_gs"`
    StandAwayL         string `json:"stand_away_l"`
    StandAwayW         string `json:"stand_away_w"`
    StandCompetitionID string `json:"stand_competition_id"`
    StandCountry       string `json:"stand_country"`
    StandDesc          string `json:"stand_desc"`
    StandGd            string `json:"stand_gd"`
    StandGroup         string `json:"stand_group"`
    StandHomeD         string `json:"stand_home_d"`
    StandHomeGa        string `json:"stand_home_ga"`
    StandHomeGp        string `json:"stand_home_gp"`
    StandHomeGs        string `json:"stand_home_gs"`
    StandHomeL         string `json:"stand_home_l"`
    StandHomeW         string `json:"stand_home_w"`
    StandID            string `json:"stand_id"`
    StandOverallD      string `json:"stand_overall_d"`
    StandOverallGa     string `json:"stand_overall_ga"`
    StandOverallGp     string `json:"stand_overall_gp"`
    StandOverallGs     string `json:"stand_overall_gs"`
    StandOverallL      string `json:"stand_overall_l"`
    StandOverallW      string `json:"stand_overall_w"`
    StandPoints        string `json:"stand_points"`
    StandPosition      string `json:"stand_position"`
    StandRecentForm    string `json:"stand_recent_form"`
    StandRound         string `json:"stand_round"`
    StandSeason        string `json:"stand_season"`
    StandStageID       string `json:"stand_stage_id"`
    StandStatus        string `json:"stand_status"`
    StandTeamID        string `json:"stand_team_id"`
    StandTeamName      string `json:"stand_team_name"`
    } `json:"teams"`
  }

  Match struct {
    APIRequestsRemaining    int     `json:"APIRequestsRemaining"`
    APIVersion              int     `json:"APIVersion"`
    Action                  string  `json:"Action"`
    ComputationTime         float64 `json:"ComputationTime"`
    DeveloperAuthentication string  `json:"DeveloperAuthentication"`
    ERROR                   string  `json:"ERROR"`
    IP                      string  `json:"IP"`
    Params                  struct {
      APIKey string `json:"APIKey"`
      Action string `json:"Action"`
      CompID string `json:"comp_id"`
    } `json:"Params"`
    ServerAddress string `json:"ServerAddress"`
    ServerName    string `json:"ServerName"`
    Matches       []struct {
      MatchCommentaryAvailable string `json:"match_commentary_available"`
      MatchCompID              string `json:"match_comp_id"`
      MatchDate                string `json:"match_date"`
      MatchEtScore             string `json:"match_et_score"`
      MatchFormattedDate       string `json:"match_formatted_date"`
      MatchFtScore             string `json:"match_ft_score"`
      MatchHtScore             string `json:"match_ht_score"`
      LegacyID                 string `json:"legacy_id"`
      MatchID                  string `json:"match_id"`
      MatchLocalteamID         string `json:"match_localteam_id"`
      MatchLocalteamName       string `json:"match_localteam_name"`
      MatchLocalteamScore      string `json:"match_localteam_score"`
      MatchSeasonBeta          string `json:"match_season_beta"`
      MatchStaticID            string `json:"match_static_id"`
      MatchStatus              string `json:"match_status"`
      MatchTime                string `json:"match_time"`
      MatchTimer               string `json:"match_timer"`
      MatchVenueBeta           string `json:"match_venue_beta"`
      MatchVenueCityBeta       string `json:"match_venue_city_beta"`
      MatchVenueIDBeta         string `json:"match_venue_id_beta"`
      MatchVisitorteamID       string `json:"match_visitorteam_id"`
      MatchVisitorteamName     string `json:"match_visitorteam_name"`
      MatchVisitorteamScore    string `json:"match_visitorteam_score"`
      MatchWeekBeta            string `json:"match_week_beta"`
    } `json:"matches"`
  }



)	

/*

APIRequestsRemaining int    `json:"APIRequestsRemaining"`
  APIVersion           int    `json:"APIVersion"`
  Action               string `json:"Action"`
  Competition          []struct {
    ID     string `json:"id"`
    Name   string `json:"name"`
    Region string `json:"region"`
  } `json:"Competition"`
  ComputationTime         float64 `json:"ComputationTime"`
  DeveloperAuthentication string  `json:"DeveloperAuthentication"`
  ERROR                   string  `json:"ERROR"`
  IP                      string  `json:"IP"`
  Params                  struct {
    APIKey string `json:"APIKey"`
    Action string `json:"Action"`
  } `json:"Params"`
  ServerAddress string `json:"ServerAddress"`
  ServerName    string `json:"ServerName"`

{
  "APIVersion": 1,
  "APIRequestsRemaining": 991,
  "DeveloperAuthentication": "TRUE",
  "Competition": [
    {
      "id": "1204",
      "name": "Premier League",
      "region": "England"
    }
  ],
  "Action": "competitions",
  "Params": {
    "Action": "competitions",
    "APIKey": "cd56da3a-d5f3-a7ed-3893f5f3bd7a"
  },
  "ComputationTime": 0.015830993652344,
  "IP": "61.238.46.18",
  "ERROR": "OK",
  "ServerName": "Football-API",
  "ServerAddress": "http://football-api.com/api"
}

*/