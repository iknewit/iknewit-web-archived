package matchModel

import (
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/prediction"
	"time"
)

type Match struct {
	Id              bson.ObjectId                `bson:"_id,omitempty" json:"Id"`
	HomeTeamId      bson.ObjectId                `bson:"HomeTeamId,omitempty" json:"HomeTeamId"`
	AwayTeamId      bson.ObjectId                `bson:"AwayTeamId,omitempty" json:"AwayTeamId"`
	HomeTeamName    string                       `bson:"HomeTeamName,omitempty" json:"HomeTeamName"`
	AwayTeamName    string                       `bson:"AwayTeamName,omitempty" json:"AwayTeamName"`
	KickoffAt       time.Time                    `bson:"KickoffAt" json:"KickoffAt"`
	CompetitionId   bson.ObjectId                `bson:"CompetitionId,omitempty" json:"CompetitionId"`
	CompetitionName string                       `bson:"CompetitionName,omitempty" json:"CompetitionName"`
	HomeTeamScore   int                          `bson:"HomeTeamScore" json:"HomeTeamScore"`
	AwayTeamScore   int                          `bson:"AwayTeamScore" json:"AwayTeamScore"`
	MatchStatus     string                       `bson:"MatchStatus" json:"MatchStatus"`
	Predictions     []predictionModel.Prediction `bson:"Predictions" json:"Predictions"`
    MatchID   		string						 `bson:"MatchID" json:"MatchID"`
}
