package ratingModel

import (
	"gopkg.in/mgo.v2/bson"
)

type Rating struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"Id"`
	TotalScore			 int           `bson:"TotalScore" json:"Total"`
}
