package leaderboardModel

import (
	"gopkg.in/mgo.v2/bson"
)

type (
	Leaderboard struct {
		Id          bson.ObjectId `bson:"_id,omitempty"`
		DisplayName string        `bson:"DisplayName" json:"DisplayName"`
		Email       string        `bson:"Email" json:"Email"`
		Level       int           `bson:"Level" json:"Level"`
		PowerPoint  int           `bson:"PowerPoint" json:"PowerPoint"`
		Coins       int           `bson:"Coins" json:"Coins"`
		Rank        int           `bson:"Rank" json:"Rank"`
	}
)
