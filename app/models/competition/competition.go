package competitionModel

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Competition struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	Name       string        `bson:"Name,omitempty" json:"Name"`
	Icon       string        `bson:"Icon" json:"Icon"`
	StartDate  time.Time     `bson:"StartDate" json:"StartDate"`
	EndDate    time.Time     `bson:"EndDate" json:"EndDate"`
	ShareCode  string        `bson:"ShareCode" json:"ShareCode"`
	CreateBy   bson.ObjectId `bson:"CreateBy"`
	IsDeleted  bool          `bson:"IsDeleted" json:"IsDeleted"`
	IsPrivate  bool          `bson:"IsPrivate" json:"IsPrivate"`
	CreateTime time.Time     `bson:"CreateTime" json:"CreateTime"`
}

type CompetitionShareCode struct {
	ShareCode string `bson:"ShareCode" json:"ShareCode"`
}

/*
type CompetitionMember struct {
    Id          bson.ObjectId   `bson:"_id,omitempty"`
    IsBlocked   bool            `bson:"IsBlocked" json:"IsBlocked"`
}
*/
