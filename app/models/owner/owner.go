package ownerModel

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type (
	Owner struct {
		UserId     bson.ObjectId `bson:"_id,omitempty"`
		CreateTime time.Time
	}
)
