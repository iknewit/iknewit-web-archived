package predictionModel

import (
	"gopkg.in/mgo.v2/bson"
)

type Prediction struct {
	UserId         bson.ObjectId `bson:"UserId" json:"UserId"`
	PredictionType string        `bson:"PredictionType" json:"PredictionType"`
	GameResult     string        `bson:"GameResult" json:"GameResult"`
	HomeTeamScore  int           `bson:"HomeTeamScore" json:"HomeTeamScore"`
	AwayTeamScore  int           `bson:"AwayTeamScore" json:"AwayTeamScore"`
	Result         string        `bson:"Result" json:"Result"`
	PowerPoint     int           `bson:"PowerPoint" json:"PowerPoint"`
	Coins          int           `bson:"Coins" json:"Coins"`
}
