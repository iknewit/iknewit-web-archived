package leagueModel

import (
	"gopkg.in/mgo.v2/bson"
)

type (
	League struct {
		Id   bson.ObjectId `bson:"_id,omitempty"`
		Name string
	}
)
