package teamModel

import (
	"gopkg.in/mgo.v2/bson"
)

type Team struct {
	Id                   bson.ObjectId `bson:"_id,omitempty"`
	Name                 string        `bson:"Name" json:"Name"`
	Abbrev               string        `bson:"Abbrev" json:"Abbrev"`
	PrimaryCompetitionId bson.ObjectId `bson:"PrimaryCompetitionId,omitempty"`
	Slug                 string        `bson:"Slug" json:"Slug"`
	CrestPathPrefix      string        `bson:"CrestPathPrefix" json:"CrestPathPrefix"`
	CrestSmallPath       string        `bson:"CrestSmallPath" json:"CrestSmallPath"`
	CrestMediumPath      string        `bson:"CrestMediumPath" json:"CrestMediumPath"`
}
