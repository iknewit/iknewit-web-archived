package achievementModel

import (
	"gopkg.in/mgo.v2/bson"
)

type Achievement struct {
	Id          bson.ObjectId `bson:"_id,omitempty" json:"Id"`
	DisplayName string        `bson:"DisplayName" json:"DisplayName"`
	Thumbnail   string        `bson:"Thumbnail" json:"Thumbnail"`
}

type AchievementIds struct {
	AchievementId bson.ObjectId `bson:"AchievementId" json:"Id"`
}
