package transactionModel

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type Transaction struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"Id"`
	PowerPoint int           `bson:"PowerPoint" json:"PowerPoint"`
	Coins      int           `bson:"Coins" json:"Coins"`
	UserId     bson.ObjectId `bson:"UserId" json:"UserId"`
	MatchId    bson.ObjectId `bson:"MatchId" json:"MatchId"`
	Type       string        `bson:"Type" json:"Type"`
	KickoffAt  time.Time     `bson:"KickoffAt" json:"KickoffAt"`
	CreateTime time.Time     `bson:"Createtime" json:"Createtime"`
}

/*
PredictScore,
PredictScoreResult,
PredictScoreResultCloseness,
PredictScoreBonus5,
PredictScoreBonus20,
PredictHomeDrawAway,
PredictHomeDrawAwayResult,
PredictHomeDrawAwayBonus,
Challenge,
ChallengeResult,
Achievement
*/
