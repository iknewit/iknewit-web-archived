package userModel

import (
	"gopkg.in/mgo.v2/bson"
	"iknewit-web/app/models/achievement"
	"time"
)

type (
	User struct {
		Id             bson.ObjectId                     `bson:"_id,omitempty"`
		DisplayName    string                            `bson:"DisplayName" json:"DisplayName"`
		Email          string                            `bson:"Email" json:"Email"`
		Password       string                            `bson:"Password" json:"Password"`
		HashedPassword []byte                            `bson:"HashedPassword" json:"HashedPassword"`
		UserType       int                               `bson:"UserType" json:"UserType"`
		Level          int                               `bson:"Level" json:"Level"`
		PowerPoint     int                               `bson:"PowerPoint" json:"PowerPoint"`
		Coins          int                               `bson:"Coins" json:"Coins"`
		ProfilePhoto   string                            `bson:"ProfilePhoto" json:"ProfilePhoto"`
		Gender         int                               `bson:"Gender" json:"Gender"`
		Rank           int                               `bson:"Rank" json:"Rank"`
		Ranks          []UserRank                        `bson:"Ranks" json:"Ranks"`
		Predictions    []UserPrediction                  `bson:"Predictions" json:"Predictions"`
		SupportTeam    bson.ObjectId                     `bson:"SupportTeam,omitempty" json:"SupportTeam"`
		AccessToken    string                            `bson:"AccessToken" json:"AccessToken"`
		Achievements   []achievementModel.AchievementIds `bson:"Achievements" json:"Achievements"`
		Competitions   []UserCompetition                 `bson:"Competitions" json:"Competitions"`
		LastUpdateTime time.Time                         `bson:"LastUpdateTime" json:"LastUpdateTime"`
		LastLoginTime  time.Time                         `bson:"LastLoginTime" json:"LastLoginTime"`
		CreateTime     time.Time                         `bson:"CreateTime" json:"CreateTime"`
		IsConfirmed    bool                              `bson:"IsConfirmed" json:"IsConfirmed"`
	}
)

type (
	UserProfile struct {
		Id           bson.ObjectId    `bson:"_id,omitempty"`
		DisplayName  string           `bson:"DisplayName" json:"DisplayName"`
		Email        string           `bson:"Email" json:"Email"`
		Level        int              `bson:"Level" json:"Level"`
		PowerPoint   int              `bson:"PowerPoint" json:"PowerPoint"`
		Coins        int              `bson:"Coins" json:"Coins"`
		ProfilePhoto string           `bson:"ProfilePhoto" json:"ProfilePhoto"`
		Rank         int              `bson:"Rank" json:"Rank"`
		Predictions  []UserPrediction `bson:"Predictions" json:"Predictions"`
	}
)

type (
	UserPassword struct {
		CurrentPassword string `bson:"CurrentPassword" json:"CurrentPassword"`
		NewPassword     string `bson:"NewPassword" json:"NewPassword"`
	}
)

type (
	UserAchievement struct {
		Id           bson.ObjectId                     `bson:"_id,omitempty"`
		Achievements []achievementModel.AchievementIds `bson:"Achievements" json:"Achievements"`
	}
)

type UserPrediction struct {
	Type      string  `bson:"Type" json:"Type"` // Score or HomeDrawAway
	Count     int     `bson:"Count" json:"Count"`
	Finished  int     `bson:"Finished" json:"Finished"`
	Win       int     `bson:"Win" json:"Win"`
	Closeness int     `bson:"Closeness" json:"Closeness"`
	Accuracy  float32 `bson:"Accuracy" json:"Accuracy"`
}

type UserRank struct {
	Id      bson.ObjectId `bson:"Id,omitempty"`
	Ranking int           `bson:"Ranking" json:"Ranking"`
}

type UserCompetition struct {
	CompetitionId bson.ObjectId `bson:"CompetitionId,omitempty"`
	IsBlocked     bool          `bson:"IsBlocked" json:"IsBlocked"`
}
